function post(url, data, callbackSuccess, callbackFail){
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {

        // if POST success
        if (this.readyState == 4 && this.status == 200) {
            if (callbackSuccess) {
                callbackSuccess(this.responseText);
            }
        }
        // if POST failed.
        else if (this.readyState == 4 && this.status != 200){
            if (callbackFail) {
                callbackFail(this.responseText);
            }
        }
    };
    xhttp.open("POST", url);
    xhttp.send(data);
}

function initFormById(formId, callbackSuccess, callbackFail){
    var frm = document.getElementById(formId);
    frm.addEventListener('submit',function(event){
        event.preventDefault();
        var url = this.getAttribute('action');
        var data = new FormData(frm);

        post(url, data, callbackSuccess, callbackFail);
    },false);
}

function defaultCallbackSuccess(responseText){
    var res = JSON.parse(responseText);
    console.log(res);
    if (res.result === 'success') {
        alert(res.message);
        window.location.reload();
    }
    else if (res.result === 'fail'){
        defaultCallbackFail(responseText);
    }
}

function defaultCallbackFail(responseText){
    console.log(responseText);
    var res = JSON.parse(responseText);
    alert(res.message);
}
