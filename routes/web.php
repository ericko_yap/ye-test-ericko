<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Auth;

Route::get('/', function () {
    return redirect('home');

});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::prefix('guest')->group(function() {
    Route::prefix('find-order')->group(function(){
        Route::post('find', 'GuestController@findOrder')->name('guest.find-order.find');
        Route::get('view', 'GuestController@viewFindOrder')->name('guest.find-order.view');

        Route::prefix('detail')->group(function(){
            Route::get('get-list/{order_data_id}','OrderDataController@detailGetList')->name('guest.find-order.detail.get-list');
        });
    });
});

Route::group(['middleware' => ['auth']], function () {
    Route::prefix('customer')->group(function(){
        Route::get('get/{customer_id}','CustomerController@get')->name('customer.get');
        Route::get('get-list','CustomerController@getList')->name('customer.get-list');
        Route::post('create','CustomerController@create')->name('customer.create');
        Route::post('update/{customer_id}','CustomerController@update')->name('customer.update');
        Route::post('delete','CustomerController@delete')->name('customer.delete');

        Route::prefix('view')->group(function(){
            Route::get('/', 'CustomerController@view')->name('customer.view');
            Route::get('create', 'CustomerController@viewCreate')->name('customer.view.create');
            Route::get('update/{customer_id}', 'CustomerController@viewUpdate')->name('customer.view.update');
        });
    });

    Route::prefix('storage')->group(function(){
        Route::get('get/{storage_id}','StorageController@get')->name('storage.get');
        Route::get('get-list','StorageController@getList')->name('storage.get-list');
        Route::post('create','StorageController@create')->name('storage.create');
        Route::post('update/{storage_id}','StorageController@update')->name('storage.update');
        Route::post('delete','StorageController@delete')->name('storage.delete');

        Route::prefix('view')->group(function(){
            Route::get('/', 'StorageController@view')->name('storage.view');
            Route::get('create', 'StorageController@viewCreate')->name('storage.view.create');
            Route::get('update/{storage_id}', 'StorageController@viewUpdate')->name('storage.view.update');
        });
    });

    Route::prefix('item-type')->group(function(){
        Route::get('get/{item_type_id}','ItemTypeController@get')->name('item-type.get');
        Route::get('get-list','ItemTypeController@getList')->name('item-type.get-list');
        Route::post('create','ItemTypeController@create')->name('item-type.create');
        Route::post('update/{item_type_id}','ItemTypeController@update')->name('item-type.update');
        Route::post('delete','ItemTypeController@delete')->name('item-type.delete');

        Route::prefix('view')->group(function(){
            Route::get('/', 'ItemTypeController@view')->name('item-type.view');
            Route::get('create', 'ItemTypeController@viewCreate')->name('item-type.view.create');
            Route::get('update/{item_type_id}', 'ItemTypeController@viewUpdate')->name('item-type.view.update');
        });
    });

    Route::prefix('order')->group(function(){
        Route::prefix('input')->group(function(){
            Route::get('get/{order_input_id}','OrderInputController@get')->name('order.input.get');
            Route::get('get-list','OrderInputController@getList')->name('order.input.get-list');
            Route::post('create','OrderInputController@create')->name('order.input.create');
            Route::post('update/{order_input_id}','OrderInputController@update')->name('order.input.update');
            Route::post('delete','OrderInputController@delete')->name('order.input.delete');

            Route::prefix('view')->group(function(){
                Route::get('/', 'OrderInputController@view')->name('order.input.view');
                Route::get('create','OrderInputController@viewCreate')->name('order.input.view.create');
                Route::get('update/{order_input_id}','OrderInputController@viewUpdate')->name('order.input.view.update');
            });
        });
        Route::prefix('output')->group(function(){
            Route::get('get/{order_output_id}','OrderOutputController@get')->name('order.output.get');
            Route::get('get-list','OrderOutputController@getList')->name('order.output.get-list');
            Route::post('create','OrderOutputController@create')->name('order.output.create');
            Route::post('update/{order_output_id}','OrderOutputController@update')->name('order.output.update');
            Route::post('delete','OrderOutputController@delete')->name('order.output.delete');

            Route::prefix('view')->group(function(){
                Route::get('/', 'OrderOutputController@view')->name('order.output.view');
                Route::get('create/{order_input_id}','OrderOutputController@viewCreate')->name('order.output.view.create');
                Route::get('update/{order_output_id}','OrderOutputController@viewUpdate')->name('order.output.view.update');
            });
        });
        Route::prefix('data')->group(function(){
            Route::get('get/{order_data_id}','OrderDataController@get')->name('order.data.get');
            Route::post('update/{order_data_id}','OrderDataController@update')->name('order.data.update');

            Route::prefix('detail')->group(function(){
                Route::get('get/{order_data_detail_id}','OrderDataController@detailGet')->name('order.data.detail.get');
                Route::get('get-list/{order_data_id}','OrderDataController@detailGetList')->name('order.data.detail.get-list');
                Route::get('update/{order_data_detail_id}','OrderDataController@detailUpdate')->name('order.data.detail.update');

                Route::prefix('view')->group(function(){
                    Route::get('update/{order_data_id}','OrderDataController@detailViewUpdate')->name('order.data.view.update');
                });
            });

            Route::prefix('view')->group(function(){
                Route::get('{order_data_id}', 'OrderDataController@view')->name('order.data.view');
                Route::get('update/{order_data_id}','OrderDataController@viewUpdate')->name('order.data.view.update');
            });
        });
    });

    Route::prefix('invoice')->group(function(){
        Route::get('get/{invoice_id}','InvoiceController@get')->name('invoice.get');
        Route::get('get-list','InvoiceController@getList')->name('invoice.get-list');
        Route::post('create','InvoiceController@create')->name('invoice.create');
        Route::post('update/{invoice_id}','InvoiceController@update')->name('invoice.update');
        Route::post('delete','InvoiceController@delete')->name('invoice.delete');

        Route::prefix('view')->group(function(){
            Route::get('/', 'InvoiceController@view')->name('invoice.view');
            Route::get('create/{order_output_id}','InvoiceController@viewCreate')->name('invoice.view.create');
            Route::get('update/{invoice_id}','InvoiceController@viewUpdate')->name('invoice.view.update');
        });
    });
});


