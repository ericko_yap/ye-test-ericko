How to install:
1. Pull from git
2. Run "composer dump-autoload"
3. Create new DB: "ye-test-ericko"
4. Copy .env-example to new file and name it as .env
5. Migrate and seed

Set Postman Collection:
1. Import collection from "docs/YouExpressErickoTest.postman_collection.json"
2. Set Environment (so {{url}} works.)

NOTES:
1. Untuk collection, kalau tidak jalan, comment group middleware di web.php

Getting Started as User/Admin:
1. Buat akun baru dari tombol "Register"
2. Lakukan apapun yang diinginkan.