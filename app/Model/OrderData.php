<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class OrderData extends Model
{
    use SoftDeletes;

    public function customer(){
        return $this->belongsTo('App\Model\Customer', 'customer_id')->withTrashed();
    }

    public function storage(){
        return $this->belongsTo('App\Model\Storage', 'storage_id')->withTrashed();
    }

    public function details(){
        return $this->hasMany('App\Model\OrderDataDetail');
    }
}
