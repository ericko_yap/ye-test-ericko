<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserLog extends Model
{
    use SoftDeletes;

    public function user(){
        return $this->belongsTo('App\Model\User', 'user_id');
    }

    public function customer(){
        return $this->belongsTo('App\Model\Customer', 'customer_id');
    }

    public function orderData(){
        return $this->belongsTo('App\Model\OrderData', 'order_data_id');
    }

    public function orderInput(){
        return $this->belongsTo('App\Model\OrderInput', 'order_input_id');
    }

    public function orderOutput(){
        return $this->belongsTo('App\Model\OrderOutput', 'order_output_id');
    }
}
