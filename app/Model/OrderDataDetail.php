<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class OrderDataDetail extends Model
{
    use SoftDeletes;

    public function orderData(){
        return $this->belongsTo('App\Model\OrderData', 'order_data_id');
    }

    public function itemType(){
        return $this->belongsTo('App\Model\ItemType', 'item_type_id');
    }
}
