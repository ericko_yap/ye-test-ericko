<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class OrderOutput extends Model
{
    use SoftDeletes;

    public function orderData(){
        return $this->belongsTo('App\Model\OrderData','order_data_id');
    }

    public function orderInput(){
        return $this->belongsTo('App\Model\OrderInput', 'order_input_id');
    }

    public function invoice(){
        return $this->hasOne('App\Model\Invoice');
    }
}
