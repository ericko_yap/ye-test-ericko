<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Customer extends Model
{
    use SoftDeletes;

    public function orderData(){
        return $this->hasMany('App\Model\OrderData');
    }

    public function invoices(){
        return $this->hasMany('App\Model\Invoice');
    }

    public function customerLog(){
        return $this->hasMany('App\Model\UserLog');
    }
}
