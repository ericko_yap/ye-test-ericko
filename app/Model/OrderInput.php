<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class OrderInput extends Model
{
    use SoftDeletes;

    public function orderData(){
        return $this->belongsTo('App\Model\OrderData','order_data_id');
    }

    public function orderOutput(){
        return $this->hasOne('App\Model\OrderOutput');
    }

    public function invoice(){
        return $this->hasOne('App\Model\Invoice');
    }
}
