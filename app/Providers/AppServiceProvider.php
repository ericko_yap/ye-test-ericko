<?php

namespace App\Providers;

use App\Model\Customer;
use App\Model\Invoice;
use App\Model\ItemType;
use App\Model\OrderData;
use App\Model\OrderInput;
use App\Model\OrderOutput;
use App\Model\Storage;
use App\Observers\CustomerObserver;
use App\Observers\InvoiceObserver;
use App\Observers\ItemTypeObserver;
use App\Observers\OrderDataObserver;
use App\Observers\OrderInputObserver;
use App\Observers\OrderOutputObserver;
use App\Observers\StorageObserver;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Customer::observe(CustomerObserver::class);
        Storage::observe(StorageObserver::class);
        ItemType::observe(ItemTypeObserver::class);
        OrderInput::observe(OrderInputObserver::class);
        OrderOutput::observe(OrderOutputObserver::class);
        Invoice::observe(InvoiceObserver::class);
    }
}
