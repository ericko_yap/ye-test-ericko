<?php

namespace App\Observers;

use App\Http\Traits\UserLogTrait;
use App\Model\OrderOutput;

class OrderOutputObserver
{
    use UserLogTrait;
    /**
     * Handle the order output "created" event.
     *
     * @param  \App\Model\OrderOutput  $orderOutput
     * @return void
     */
    public function created(OrderOutput $orderOutput)
    {
        $orderOutputLog = [
            'data' => $orderOutput,
            'action' => 'create'
        ];

        $this->createOrderOutputLog($orderOutputLog);
    }

    /**
     * Handle the order output "updated" event.
     *
     * @param  \App\Model\OrderOutput  $orderOutput
     * @return void
     */
    public function updated(OrderOutput $orderOutput)
    {
        $orderOutputLog = [
            'data' => $orderOutput,
            'action' => 'update'
        ];

        $this->createOrderOutputLog($orderOutputLog);
    }

    /**
     * Handle the order output "deleted" event.
     *
     * @param  \App\Model\OrderOutput  $orderOutput
     * @return void
     */
    public function deleted(OrderOutput $orderOutput)
    {
        $orderOutputLog = [
            'data' => $orderOutput,
            'action' => 'delete'
        ];

        $this->createOrderOutputLog($orderOutputLog);
    }

    /**
     * Handle the order output "restored" event.
     *
     * @param  \App\Model\OrderOutput  $orderOutput
     * @return void
     */
    public function restored(OrderOutput $orderOutput)
    {
        //
    }

    /**
     * Handle the order output "force deleted" event.
     *
     * @param  \App\Model\OrderOutput  $orderOutput
     * @return void
     */
    public function forceDeleted(OrderOutput $orderOutput)
    {
        //
    }
}
