<?php

namespace App\Observers;

use App\Http\Traits\UserLogTrait;
use App\Model\Invoice;

class InvoiceObserver
{
    use UserLogTrait;
    /**
     * Handle the invoice "created" event.
     *
     * @param  \App\Model\Invoice  $invoice
     * @return void
     */
    public function created(Invoice $invoice)
    {
        $invoiceLog = [
            'data' => $invoice,
            'action' => 'create'
        ];

        $this->createInvoiceLog($invoiceLog);
    }

    /**
     * Handle the invoice "updated" event.
     *
     * @param  \App\Model\Invoice  $invoice
     * @return void
     */
    public function updated(Invoice $invoice)
    {
        $invoiceLog = [
            'data' => $invoice,
            'action' => 'update'
        ];

        $this->createInvoiceLog($invoiceLog);
    }

    /**
     * Handle the invoice "deleted" event.
     *
     * @param  \App\Model\Invoice  $invoice
     * @return void
     */
    public function deleted(Invoice $invoice)
    {
        $invoiceLog = [
            'data' => $invoice,
            'action' => 'delete'
        ];

        $this->createInvoiceLog($invoiceLog);
    }

    /**
     * Handle the invoice "restored" event.
     *
     * @param  \App\Model\Invoice  $invoice
     * @return void
     */
    public function restored(Invoice $invoice)
    {
        //
    }

    /**
     * Handle the invoice "force deleted" event.
     *
     * @param  \App\Model\Invoice  $invoice
     * @return void
     */
    public function forceDeleted(Invoice $invoice)
    {
        //
    }
}
