<?php

namespace App\Observers;

use App\Http\Traits\UserLogTrait;
use App\Model\ItemType;

class ItemTypeObserver
{
    use UserLogTrait;
    /**
     * Handle the item type "created" event.
     *
     * @param  \App\Model\ItemType  $itemType
     * @return void
     */
    public function created(ItemType $itemType)
    {
        $itemTypeLog = [
            'data' => $itemType,
            'action' => 'create'
        ];

        $this->createItemTypeLog($itemTypeLog);
    }

    /**
     * Handle the item type "updated" event.
     *
     * @param  \App\Model\ItemType  $itemType
     * @return void
     */
    public function updated(ItemType $itemType)
    {
        $itemTypeLog = [
            'data' => $itemType,
            'action' => 'update'
        ];

        $this->createItemTypeLog($itemTypeLog);
    }

    /**
     * Handle the item type "deleted" event.
     *
     * @param  \App\Model\ItemType  $itemType
     * @return void
     */
    public function deleted(ItemType $itemType)
    {
        $itemTypeLog = [
            'data' => $itemType,
            'action' => 'delete'
        ];

        $this->createItemTypeLog($itemTypeLog);
    }

    /**
     * Handle the item type "restored" event.
     *
     * @param  \App\Model\ItemType  $itemType
     * @return void
     */
    public function restored(ItemType $itemType)
    {
        //
    }

    /**
     * Handle the item type "force deleted" event.
     *
     * @param  \App\Model\ItemType  $itemType
     * @return void
     */
    public function forceDeleted(ItemType $itemType)
    {
        //
    }
}
