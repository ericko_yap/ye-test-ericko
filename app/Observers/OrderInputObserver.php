<?php

namespace App\Observers;

use App\Http\Traits\UserLogTrait;
use App\Model\OrderInput;

class OrderInputObserver
{
    use UserLogTrait;
    /**
     * Handle the order input "created" event.
     *
     * @param  \App\Model\OrderInput  $orderInput
     * @return void
     */
    public function created(OrderInput $orderInput)
    {
        $orderInputLog = [
            'data' => $orderInput,
            'action' => 'create'
        ];

        $this->createOrderInputLog($orderInputLog);
    }

    /**
     * Handle the order input "updated" event.
     *
     * @param  \App\Model\OrderInput  $orderInput
     * @return void
     */
    public function updated(OrderInput $orderInput)
    {
        $orderInputLog = [
            'data' => $orderInput,
            'action' => 'update'
        ];

        $this->createOrderInputLog($orderInputLog);
    }

    /**
     * Handle the order input "deleted" event.
     *
     * @param  \App\Model\OrderInput  $orderInput
     * @return void
     */
    public function deleted(OrderInput $orderInput)
    {
        $orderInputLog = [
            'data' => $orderInput,
            'action' => 'delete'
        ];

        $this->createOrderInputLog($orderInputLog);
    }

    /**
     * Handle the order input "restored" event.
     *
     * @param  \App\Model\OrderInput  $orderInput
     * @return void
     */
    public function restored(OrderInput $orderInput)
    {
        //
    }

    /**
     * Handle the order input "force deleted" event.
     *
     * @param  \App\Model\OrderInput  $orderInput
     * @return void
     */
    public function forceDeleted(OrderInput $orderInput)
    {
        //
    }
}
