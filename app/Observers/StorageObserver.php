<?php

namespace App\Observers;

use App\Http\Traits\UserLogTrait;
use App\Model\Storage;

class StorageObserver
{
    use UserLogTrait;
    /**
     * Handle the storage "created" event.
     *
     * @param  \App\Model\Storage  $storage
     * @return void
     */
    public function created(Storage $storage)
    {
        $storageLog = [
            'data' => $storage,
            'action' => 'create'
        ];

        $this->createStorageLog($storageLog);
    }

    /**
     * Handle the storage "updated" event.
     *
     * @param  \App\Model\Storage  $storage
     * @return void
     */
    public function updated(Storage $storage)
    {
        $storageLog = [
            'data' => $storage,
            'action' => 'update'
        ];

        $this->createStorageLog($storageLog);
    }

    /**
     * Handle the storage "deleted" event.
     *
     * @param  \App\Model\Storage  $storage
     * @return void
     */
    public function deleted(Storage $storage)
    {

        $storageLog = [
            'data' => $storage,
            'action' => 'delete'
        ];

        $this->createStorageLog($storageLog);
    }

    /**
     * Handle the storage "restored" event.
     *
     * @param  \App\Model\Storage  $storage
     * @return void
     */
    public function restored(Storage $storage)
    {
        //
    }

    /**
     * Handle the storage "force deleted" event.
     *
     * @param  \App\Model\Storage  $storage
     * @return void
     */
    public function forceDeleted(Storage $storage)
    {
        //
    }
}
