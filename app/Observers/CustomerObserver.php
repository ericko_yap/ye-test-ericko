<?php

namespace App\Observers;

use App\Http\Traits\UserLogTrait;
use App\Model\Customer;

class CustomerObserver
{
    use UserLogTrait;
    /**
     * Handle the customer "created" event.
     *
     * @param  \App\Model\Customer  $customer
     * @return void
     */
    public function created(Customer $customer)
    {
        $customerLog = [
            'data' => $customer,
            'action' => 'create'
        ];

        $this->createCustomerLog($customerLog);
    }

    /**
     * Handle the customer "updated" event.
     *
     * @param  \App\Model\Customer  $customer
     * @return void
     */
    public function updated(Customer $customer)
    {
        $customerLog = [
            'data' => $customer,
            'action' => 'update'
        ];

        $this->createCustomerLog($customerLog);
    }

    /**
     * Handle the customer "deleted" event.
     *
     * @param  \App\Model\Customer  $customer
     * @return void
     */
    public function deleted(Customer $customer)
    {
        $customerLog = [
            'data' => $customer,
            'action' => 'delete'
        ];

        $this->createCustomerLog($customerLog);
    }

    /**
     * Handle the customer "restored" event.
     *
     * @param  \App\Model\Customer  $customer
     * @return void
     */
    public function restored(Customer $customer)
    {
        //
    }

    /**
     * Handle the customer "force deleted" event.
     *
     * @param  \App\Model\Customer  $customer
     * @return void
     */
    public function forceDeleted(Customer $customer)
    {
        //
    }
}
