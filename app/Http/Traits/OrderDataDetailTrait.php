<?php
namespace App\Http\Traits;

use App\Model\OrderData;
use App\Model\OrderDataDetail;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use League\Flysystem\Exception;

trait OrderDataDetailTrait {

    public function orderDataDetailGet($order_data_detail_id){
        $orderInput = OrderDataDetail::where('id', $order_data_detail_id)->first();

        return $orderInput;
    }

    public function orderDataDetailGetList($order_data_id, Request $request){
        $orderDataDetailList = OrderDataDetail::where('order_data_id', $order_data_id)->get();
        foreach($orderDataDetailList as $detail){
            $detail->item_type_name = $detail->itemType->type;
        }
        return $orderDataDetailList;
    }

    public function orderDataDetailMultipleCreate($order_data_id, Request $request){
        // form format: details[][<property>]
        $orderDataDetails = $request->details;

        $rules = [
            'details.*.item_name' => 'required',
            'details.*.item_type_id' => 'required|exists:item_types,id',
            'details.*.quantity' => 'required|integer',
            'details.*.unit' => 'required',
            'details.*.notes' => ''
        ];

        $res = $this->validateInput($request, $rules);

        // if validation fails, return error.
        if ($res['result'] === 'fail'){
            return $res;
        }

        try {
            DB::beginTransaction();

            foreach ($orderDataDetails as $detail) {
                $orderDataDetail = new OrderDataDetail;
                $orderDataDetail->order_data_id = $order_data_id;
                $orderDataDetail->item_name =  $detail['item_name'];
                $orderDataDetail->item_type_id = $detail['item_type_id'];
                $orderDataDetail->quantity = $detail['quantity'];
                $orderDataDetail->unit = $detail['unit'];
                $orderDataDetail->notes = isset($detail['notes']) ? $detail['notes'] : '';
                $orderDataDetail->save();
            }

            DB::commit();

            return [
                'result' => 'success',
                'message' => 'Succesfully added order data details'
            ];
        }
        catch (Exception $ex){
            DB::rollBack();

            return [
                'result' => 'fail',
                'message' => $ex->getMessage()
            ];
        }
    }

    public function orderDataDetailCreate(Request $request){
        $rules = [
            'order_data_id' => 'required|exists:order_data,id',
            'item_name' => 'required',
            'item_type_id' => 'required|exists:item_types,id',
            'quantity' => 'required|integer',
            'unit' => 'required',
            'notes' => ''
        ];

        $res = $this->validateInput($request, $rules);

        // if validation fails, return error.
        if ($res['result'] === 'fail'){
            return $res;
        }

        // if validation successes, input data.
        try {
            DB::beginTransaction();
            $orderDataDetail = new OrderDataDetail;
            $orderDataDetail->order_data_id = $request->order_data_id;
            $orderDataDetail->item_name = $request->item_name;
            $orderDataDetail->item_type_id = $request->item_type_id;
            $orderDataDetail->quantity = $request->quantity;
            $orderDataDetail->unit = $request->unit;
            $orderDataDetail->notes = isset($request->notes) ? $request->notes : '';
            $orderDataDetail->save();

            DB::commit();

            return [
                'result' => 'success',
                'data' => $orderDataDetail,
                'message' => 'Successfully added new order data detail'
            ];
        }
        catch (Exception $ex){
            DB::rollBack();

            return [
                'result' => 'fail',
                'message' => $ex->getMessage()
            ];
        }
    }

    public function orderDataDetailUpdate($order_data_detail_id, $request){
        $rules = [
            'item_name' => 'required',
            'item_type_id' => 'required|exists:item_types,id',
            'quantity' => 'required|integer',
            'unit' => 'required',
            'notes' => ''
        ];

        $res = $this->orderDataDetailValidateInput($request, $rules);

        // if validation fails, return error.
        if ($res['result'] === 'fail'){
            return $res;
        }

        try {
            DB::beginTransaction();
            $orderDataDetail = $this->orderDataDetailGet($order_data_detail_id);
            if (!isset($orderDataDetail)){
                DB::rollBack();
                return [
                    'result' => 'fail',
                    'message' => 'Order Data Detail ID not found.'
                ];
            }

            $orderDataDetail->item_name = $request->item_name;
            $orderDataDetail->item_type_id = $request->item_type_id;
            $orderDataDetail->quantity = $request->quantity;
            $orderDataDetail->unit = $request->unit;

            if (isset($request->notes)) {
                $orderDataDetail->notes = $request->notes;
            }

            $orderDataDetail->save();

            DB::commit();

            return [
                'result' => 'success',
                'data' => $orderDataDetail,
                'message' => 'Successfully updated order input data.'
            ];
        }
        catch (Exception $ex){
            DB::rollBack();
            return [
                'result' => 'fail',
                'message' => $ex->getMessage()
            ];
        }
    }

    public function orderDataDetailDelete(Request $request){
        try {
            DB::beginTransaction();
            $orderDataDetail = OrderDataDetail::where('id', $request->order_data_detail_id)->first();

            if (!isset($orderDataDetail)) {
                DB::rollBack();

                return [
                    'result' => 'fail',
                    'message' => 'Data does not exist'
                ];
            }
            $orderDataDetail->delete();

            DB::commit();
            return [
                'result' => 'success',
                'message' => 'Successfully deleted order data.'
            ];

        }
        catch (Exception $ex){
            DB::rollBack();

            return [
                'result' => 'fail',
                'message' => $ex->getMessage()
            ];
        }
    }

    public function orderDataDetailValidateInput(Request $request, $rules){

        $messages = [
            'order_data_id.required' => 'Order Data ID is required',
            'order_data_id.exists' => 'Order Data ID does not exist',
            'item_name.required' => 'Item name is required',
            'item_type_id.required' => 'Item Type ID is required',
            'item_type_id.exists' => 'Item Type ID does not exist.',
            'quantity.required' => 'Quantity is required.',
            'quantity.integer' => 'Quantity must be integer.',
            'unit.required' => 'Unit is required.'
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            $errors = $validator->errors();
            $returnMsg = '';
            foreach ($errors->all() as $message) {
                $returnMsg .= $message . "<br>";
            }
            return [
                'result' => 'fail',
                'message' => $returnMsg
            ];
        }

        return [
            'result' => 'success',
            'message' => 'Success'
        ];
    }
}
