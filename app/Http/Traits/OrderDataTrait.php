<?php
namespace App\Http\Traits;

use App\Model\OrderData;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use League\Flysystem\Exception;

trait OrderDataTrait {
    use OrderDataDetailTrait;

    public function orderDataGet($order_data_id){
        $orderInput = OrderData::where('id', $order_data_id)->first();

        return $orderInput;
    }

    public function orderDataGetList(Request $request){
        $orderDataList = OrderData::all();
        return $orderDataList;
    }

    public function orderDataCreate(Request $request){
        $rules = [
            'customer_id' => 'required|exists:customers,id',
            'storage_id' => 'required|exists:storages,id',
            'notes' => ''
        ];

        $res = $this->orderDataValidateInput($request, $rules);

        // if validation fails, return error.
        if ($res['result'] === 'fail'){
            return $res;
        }

        // if validation successes, input data.
        try {
            DB::beginTransaction();
            $orderData = new OrderData;

            $orderData->customer_id = $request->customer_id;
            $orderData->storage_id = $request->storage_id;
            $orderData->status = 'RENTED';
            $orderData->date_input = Carbon::now()->toDateTimeString();
            $orderData->notes = isset($request->notes) ? $request->notes : '';
            $orderData->save();

            $orderData->code = 'ORD'.str_pad($orderData->id, 8, '0', STR_PAD_LEFT);
            $orderData->save();

            $res = $this->orderDataDetailMultipleCreate($orderData->id, $request);
            if ($res['result'] === 'fail'){
                return $res;
            }

            DB::commit();

            return [
                'result' => 'success',
                'data' => $orderData,
                'message' => 'Successfully added new order data'
            ];
        }
        catch (Exception $ex){
            DB::rollBack();

            return [
                'result' => 'fail',
                'message' => $ex->getMessage()
            ];
        }
    }

    public function orderDataUpdate($order_data_id, $request){
        $rules = [
            'status' => [
                'nullable',
                Rule::in(['RENTED','COMPLETED']),
            ],
            'notes' => '',
        ];

        $res = $this->validateInput($request, $rules);

        // if validation fails, return error.
        if ($res['result'] === 'fail'){
            return $res;
        }

        try {
            DB::beginTransaction();

            $orderData = $this->orderDataGet($order_data_id);
            $orderData->status = isset($request->status) ? $request->status : $orderData->status;
            if (isset($request->notes)) {
                $orderData->notes = $request->notes;
            }
            $orderData->save();

            DB::commit();

            return [
                'result' => 'success',
                'data' => $orderData,
                'message' => 'Successfully updated order input data.'
            ];
        }
        catch (Exception $ex){
            DB::rollBack();
            return [
                'result' => 'fail',
                'message' => $ex->getMessage()
            ];
        }
    }

    public function orderDataDelete(Request $request){
        try {
            DB::beginTransaction();
            $orderData = OrderData::where('id', $request->order_data_id)->first();

            if (!isset($orderData)) {
                DB::rollBack();

                return [
                    'result' => 'fail',
                    'message' => 'Data does not exist'
                ];
            }
            $orderData->delete();

            DB::commit();
            return [
                'result' => 'success',
                'message' => 'Successfully deleted order data.'
            ];

        }
        catch (Exception $ex){
            DB::rollBack();

            return [
                'result' => 'fail',
                'message' => $ex->getMessage()
            ];
        }
    }

    public function orderDataValidateInput(Request $request, $rules){

        $messages = [
            'customer_id.required' => 'Customer ID is required.',
            'customer_id.exists' => 'Customer does not exist, please register new customer or pick from existing customer.',
            'storage_id.required' => 'Storage ID is required',
            'storage_id.exists' => 'Storage does not exists, please register new storage or pick from existing list.'
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            $errors = $validator->errors();
            $returnMsg = '';
            foreach ($errors->all() as $message) {
                $returnMsg .= $message . "<br>";
            }
            return [
                'result' => 'fail',
                'message' => $returnMsg
            ];
        }

        return [
            'result' => 'success',
            'message' => 'Success'
        ];
    }
}
