<?php
namespace App\Http\Traits;

use App\Model\UserLog;
use Illuminate\Support\Facades\Auth;

trait UserLogTrait {
    public function createCustomerLog($log){
        $customer = $log['data'];

        switch ($log['action']) {
            case 'create':
                $userLog = new UserLog;
                $userLog->user_id = Auth::user()->id;
                $userLog->customer_id = $customer->id;
                $userLog->log = "Successfully created new customer: " . $customer->name . " (CUSTOMER_ID: " . $customer->id . ", EMAIL: " . $customer->email . ")";
                $userLog->save();
                break;
            case 'update':
                $userLog = new UserLog;
                $userLog->user_id = Auth::user()->id;
                $userLog->customer_id = $customer->id;
                $userLog->log = "Successfully updated customer: " . $customer->name . " (CUSTOMER_ID: " . $customer->id . ", EMAIL: " . $customer->email . ")";
                $userLog->save();
                break;
            case 'delete':
                $userLog = new UserLog;
                $userLog->user_id = Auth::user()->id;
                $userLog->customer_id = $customer->id;
                $userLog->log = "Successfully deleted customer: " . $customer->name . " (CUSTOMER_ID: " . $customer->id . ", EMAIL: " . $customer->email . ")";
                $userLog->save();
                break;
        }
    }

    public function createItemTypeLog($log){
        $itemType = $log['data'];

        switch ($log['action']) {
            case 'create':
                $userLog = new UserLog;
                $userLog->user_id = Auth::user()->id;
                $userLog->log = "Successfully created new item type: " . $itemType->type . " (ITEM_TYPE_ID: " . $itemType->id . ")";
                $userLog->save();
                break;
            case 'update':
                $userLog = new UserLog;
                $userLog->user_id = Auth::user()->id;
                $userLog->log = "Successfully updated item type: " . $itemType->type . " (ITEM_TYPE_ID: " . $itemType->id . ")";
                $userLog->save();
                break;
            case 'delete':
                $userLog = new UserLog;
                $userLog->user_id = Auth::user()->id;
                $userLog->log = "Successfully deleted item type: " . $itemType->type . " (ITEM_TYPE_ID: " . $itemType->id . ")";
                $userLog->save();
                break;
        }
    }

    public function createStorageLog($log){
        $storage = $log['data'];

        switch ($log['action']) {
            case 'create':
                $userLog = new UserLog;
                $userLog->user_id = Auth::user()->id;
                $userLog->log = "Successfully created new storage: " . $storage->type . " (STORAGE_ID: " . $storage->id . ")";
                $userLog->save();
                break;
            case 'update':
                $userLog = new UserLog;
                $userLog->user_id = Auth::user()->id;
                $userLog->log = "Successfully updated storage: " . $storage->type . " (STORAGE_ID: " . $storage->id . ")";
                $userLog->save();
                break;
            case 'delete':
                $userLog = new UserLog;
                $userLog->user_id = Auth::user()->id;
                $userLog->log = "Successfully deleted storage: " . $storage->type . " (STORAGE_ID: " . $storage->id . ")";
                $userLog->save();
                break;
        }
    }

    public function createOrderInputLog($log){
        $orderInput = $log['data'];

        switch ($log['action']) {
            case 'create':
                $userLog = new UserLog;
                $userLog->user_id = Auth::user()->id;
                $userLog->customer_id = $orderInput->orderData->customer_id;
                $userLog->order_input_id = $orderInput->id;
                $userLog->order_data_id = $orderInput->orderData->id;
                $userLog->log = "Successfully created new OrderInput: " . $orderInput->code . " (ORDER_INPUT_ID: " . $orderInput->id . ")";
                $userLog->save();
                break;
            case 'update':
                $userLog = new UserLog;
                $userLog->user_id = Auth::user()->id;
                $userLog->customer_id = $orderInput->orderData->customer_id;
                $userLog->order_input_id = $orderInput->id;
                $userLog->order_data_id = $orderInput->orderData->id;
                $userLog->log = "Successfully updated OrderInput: " . $orderInput->code . " (ORDER_INPUT_ID: " . $orderInput->id . ")";
                $userLog->save();
                break;
            case 'delete':
                $userLog = new UserLog;
                $userLog->user_id = Auth::user()->id;
                $userLog->customer_id = $orderInput->orderData->customer_id;
                $userLog->order_input_id = $orderInput->id;
                $userLog->order_data_id = $orderInput->orderData->id;
                $userLog->log = "Successfully deleted OrderInput: " . $orderInput->code . " (ORDER_INPUT_ID: " . $orderInput->id . ")";
                $userLog->save();
                break;
        }
    }

    public function createOrderOutputLog($log){
        $orderOutput = $log['data'];

        switch ($log['action']) {
            case 'create':
                $userLog = new UserLog;
                $userLog->user_id = Auth::user()->id;
                $userLog->customer_id = $orderOutput->orderData->customer_id;
                $userLog->order_input_id = $orderOutput->orderInput->id;
                $userLog->order_output_id = $orderOutput->id;
                $userLog->order_data_id = $orderOutput->orderData->id;
                $userLog->log = "Successfully created new OrderOutput: " . $orderOutput->code . " (ORDER_OUTPUT_ID: " . $orderOutput->id . ")";
                $userLog->save();
                break;
            case 'update':
                $userLog = new UserLog;
                $userLog->user_id = Auth::user()->id;
                $userLog->customer_id = $orderOutput->orderData->customer_id;
                $userLog->order_input_id = $orderOutput->orderInput->id;
                $userLog->order_output_id = $orderOutput->id;
                $userLog->order_data_id = $orderOutput->orderData->id;
                $userLog->log = "Successfully updated OrderOutput: " . $orderOutput->code . " (ORDER_OUTPUT_ID: " . $orderOutput->id . ")";
                $userLog->save();
                break;
            case 'delete':
                $userLog = new UserLog;
                $userLog->user_id = Auth::user()->id;
                $userLog->customer_id = $orderOutput->orderData->customer_id;
                $userLog->order_input_id = $orderOutput->orderInput->id;
                $userLog->order_output_id = $orderOutput->id;
                $userLog->order_data_id = $orderOutput->orderData->id;
                $userLog->log = "Successfully deleted OrderOutput: " . $orderOutput->code . " (ORDER_OUTPUT_ID: " . $orderOutput->id . ")";
                $userLog->save();
                break;
        }
    }

    public function createInvoiceLog($log){
        $invoice = $log['data'];

        switch ($log['action']) {
            case 'create':
                $userLog = new UserLog;
                $userLog->user_id = Auth::user()->id;
                $userLog->customer_id = $invoice->orderOutput->orderData->customer_id;
                $userLog->order_input_id = $invoice->order_input_id;
                $userLog->order_output_id = $invoice->order_output_id;
                $userLog->order_data_id = $invoice->orderOutput->orderData->id;
                $userLog->log = "Successfully created new Invoice: " . $invoice->code . " (INVOICE_ID: " . $invoice->id . "). AMT: " . $invoice->currency . ' ' . $invoice->amount . '. MINS:' . $invoice->total_minutes . ' mins. STATUS: '. $invoice->status;
                $userLog->save();
                break;
            case 'update':
                $userLog = new UserLog;
                $userLog->user_id = Auth::user()->id;
                $userLog->customer_id = $invoice->orderOutput->orderData->customer_id;
                $userLog->order_input_id = $invoice->order_input_id;
                $userLog->order_output_id = $invoice->order_output_id;
                $userLog->order_data_id = $invoice->orderOutput->orderData->id;
                $userLog->log = "Successfully updated Invoice: " . $invoice->code .  " (INVOICE_ID: " . $invoice->id . "). AMT: " . $invoice->currency . ' ' . $invoice->amount . '. MINS:' . $invoice->total_minutes . ' mins. STATUS: '. $invoice->status;
                $userLog->save();
                break;
            case 'delete':
                $userLog = new UserLog;
                $userLog->user_id = Auth::user()->id;
                $userLog->customer_id = $invoice->orderOutput->orderData->customer_id;
                $userLog->order_input_id = $invoice->order_input_id;
                $userLog->order_output_id = $invoice->order_output_id;
                $userLog->order_data_id = $invoice->orderOutput->orderData->id;
                $userLog->log = "Successfully deleted Invoice: " . $invoice->code .  " (INVOICE_ID: " . $invoice->id . "). AMT: " . $invoice->currency . ' ' . $invoice->amount . '. MINS:' . $invoice->total_minutes . ' mins. STATUS: '. $invoice->status;
                $userLog->save();
                break;
        }
    }
}
