<?php

namespace App\Http\Controllers;

use App\Model\Storage;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use League\Flysystem\Exception;

class StorageController extends Controller
{
    public function get($storage_id){
        $storage = Storage::where('id', $storage_id)->first();

        return $storage;
    }

    public function getList(Request $request){
        $storageList = Storage::all();
        return $storageList;
    }

    public function create(Request $request){

        $rules = [
            'type' => 'required',
            'capacity' => 'required',
            'description' => '',
            'location' => '',
            'price_per_hour' => 'required|integer',
            'currency' => 'required'
        ];

        $res = $this->validateInput($request, $rules);

        // if validation fails, return error.
        if ($res['result'] === 'fail'){
            return $res;
        }

        // if validation successes, input data.
        try {
            DB::beginTransaction();
            $storage = new Storage;
            $storage->type = $request->type;
            $storage->capacity = $request->capacity;
            $storage->description = isset($request->description) ? $request->description : '';
            $storage->location = isset($request->location) ? $request->location : '';
            $storage->price_per_hour = $request->price_per_hour;
            $storage->currency = $request->currency;

            $storage->save();

            // CREATE NEW STORAGE UNIQUE ID
            $storage->storage_uid = 'YES'.str_pad($storage->id, 8, '0', STR_PAD_LEFT);
            $storage->save();

            DB::commit();

            return [
                'result' => 'success',
                'message' => 'Successfully added new storage'
            ];
        }
        catch (Exception $ex){
            DB::rollBack();

            return [
                'result' => 'fail',
                'message' => $ex->getMessage()
            ];
        }
    }

    public function update($storage_id, Request $request){
        $rules = [
            'type' => 'required',
            'capacity' => 'required',
            'description' => '',
            'location' => '',
            'price_per_hour' => 'required|integer',
            'currency' => 'required'
        ];

        $res = $this->validateInput($request, $rules);

        // if validation fails, return error.
        if ($res['result'] === 'fail'){
            return $res;
        }

        // if validation successes, input data.
        try {
            DB::beginTransaction();
            $storage = $this->get($storage_id);
            $storage->type = $request->type;
            $storage->capacity = $request->capacity;
            $storage->description = isset($request->description) ? $request->description : '';
            $storage->location = isset($request->location) ? $request->location : '';
            $storage->price_per_hour = $request->price_per_hour;
            $storage->currency = $request->currency;

            $storage->save();

            DB::commit();

            return [
                'result' => 'success',
                'message' => 'Successfully updated storage data.'
            ];
        }
        catch (Exception $ex){
            DB::rollBack();

            return [
                'result' => 'fail',
                'message' => $ex->getMessage()
            ];
        }

    }

    public function delete(Request $request){
        try {
            DB::beginTransaction();
            $storage = Storage::where('id', $request->storage_id)->first();

            if (!isset($storage)) {
                DB::rollBack();

                return [
                    'result' => 'fail',
                    'message' => 'Data does not exist'
                ];
            }

            $storage->delete();

            DB::commit();
            return [
                'result' => 'success',
                'message' => 'Successfully deleted storage.'
            ];

        }
        catch (Exception $ex){
            DB::rollBack();

            return [
                'result' => 'fail',
                'message' => $ex->getMessage()
            ];
        }
    }

    public function validateInput(Request $request, $rules){

        $messages = [
            'storage_uid.required' => 'Storage UID is required.',
            'storage_uid.unique' => 'Storage UID already exists.',
            'type.required' => 'Type is required.',
            'capacity.required' => 'Capacity is required.',
            'price_per_hour.required' => 'Price per hour is required.',
            'price_per_hour.integer' => 'Price per hour must be integer.',
            'currency.required' => 'Currency must be string.'
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            $errors = $validator->errors();
            $returnMsg = '';
            foreach ($errors->all() as $message) {
                $returnMsg .= $message . "<br>";
            }
            return [
                'result' => 'fail',
                'message' => $returnMsg
            ];
        }

        return [
            'result' => 'success',
            'message' => 'Success'
        ];
    }


    public function view(){
        return view('storage.storage');
    }

    public function viewCreate(){
        return view('storage.storage-create');
    }

    public function viewUpdate($storage_id){
        $storage = $this->get($storage_id);
        return view('storage.storage-update', compact('storage'));
    }
}
