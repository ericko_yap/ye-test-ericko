<?php

namespace App\Http\Controllers;

use App\Model\Customer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use League\Flysystem\Exception;

class CustomerController extends Controller
{
    public function get($customer_id){
        $customer = Customer::where('id', $customer_id)->first();

        return $customer;
    }

    public function getList(Request $request){
        $customerList = Customer::all();
        return $customerList;
    }

    public function create(Request $request){

        $rules = [
            'id_number' => 'required|unique:customers,id_number',
            'name' => 'required',
            'email' => 'required|unique:customers,email',
            'address' => 'required',
            'phone_number' => 'required|numeric',
        ];

        $res = $this->validateInput($request, $rules);

        // if validation fails, return error.
        if ($res['result'] === 'fail'){
            return $res;
        }

        // if validation successes, input data.
        try {
            DB::beginTransaction();
            $customer = new Customer;
            $customer->id_number = $request->id_number;
            $customer->name = $request->name;
            $customer->email = $request->email;
            $customer->address = isset($request->address) ? $request->address : '';
            $customer->phone_number = $request->phone_number;

            $customer->save();

            DB::commit();

            return [
                'result' => 'success',
                'message' => 'Successfully added new customer'
            ];
        }
        catch (Exception $ex){
            DB::rollBack();

            return [
                'result' => 'fail',
                'message' => $ex->getMessage()
            ];
        }
    }

    public function update($customer_id, Request $request){
        $rules = [
            'id_number' => [
                'required',
                Rule::unique('customers')->ignore($customer_id)
            ],
            'name' => 'required',
            'email' => 'required|email',
            'address' => 'required',
            'phone_number' => 'required|numeric',
        ];

        $res = $this->validateInput($request, $rules);

        // if validation fails, return error.
        if ($res['result'] === 'fail'){
            return $res;
        }

        // if validation successes, input data.
        try {
            DB::beginTransaction();
            $customer = $this->get($customer_id);
            $customer->id_number = $request->id_number;
            $customer->name = $request->name;
            $customer->email = $request->email;
            $customer->address = isset($request->address) ? $request->address : '';
            $customer->phone_number = $request->phone_number;

            $customer->save();

            DB::commit();

            return [
                'result' => 'success',
                'message' => 'Successfully updated customer data.'
            ];
        }
        catch (Exception $ex){
            DB::rollBack();

            return [
                'result' => 'fail',
                'message' => $ex->getMessage()
            ];
        }

    }

    public function delete(Request $request){
        try {
            DB::beginTransaction();
            $customer = Customer::where('id', $request->customer_id)->first();

            if (!isset($customer)) {
                DB::rollBack();

                return [
                    'result' => 'fail',
                    'message' => 'Data does not exist'
                ];
            }
            $customer->delete();

            DB::commit();
            return [
                'result' => 'success',
                'message' => 'Successfully deleted customer'
            ];

        }
        catch (Exception $ex){
            DB::rollBack();

            return [
                'result' => 'fail',
                'message' => $ex->getMessage()
            ];
        }
    }

    public function validateInput(Request $request, $rules){

        $messages = [
            'id_number.required' => 'ID Number is required.',
            'id_number.unique' => 'ID Number already exists.',
            'name.required' => 'Name is required.',
            'email.required' => 'Email is required.',
            'email.email' => 'Email format is incorrect.',
            'email.unique' => 'Email already exists, please input another email',
            'address.required' => 'Address is required',
            'phone_number.required' => 'Phone Number is required.',
            'phone_number.numeric' => 'Phone Number must be numeric.'
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            $errors = $validator->errors();
            $returnMsg = '';
            foreach ($errors->all() as $message) {
                $returnMsg .= $message . "<br>";
            }
            return [
                'result' => 'fail',
                'message' => $returnMsg
            ];
        }

        return [
            'result' => 'success',
            'message' => 'Success'
        ];
    }

    public function view(){
        return view('customer.customer');
    }

    public function viewCreate(){
        return view('customer.customer-create');
    }

    public function viewUpdate($customer_id){
        $customer = $this->get($customer_id);
        return view('customer.customer-update', compact('customer'));
    }
}
