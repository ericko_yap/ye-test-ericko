<?php

namespace App\Http\Controllers;

use App\Http\Traits\OrderDataTrait;
use App\Model\Invoice;
use App\Model\OrderOutput;
use App\Model\Storage;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use League\Flysystem\Exception;

class InvoiceController extends Controller
{
    use OrderDataTrait;

    public function get($invoice_id){
        $invoice = Invoice::where('id', $invoice_id)->first();

        return $invoice;
    }

    public function getList(Request $request){
        $invoiceList = Invoice::all();
        foreach($invoiceList as $inv){
            $inv->customer = $inv->orderOutput->orderData->customer->name;
            $inv->order_data_id = $inv->orderOutput->orderData->id;
            $inv->date_input = $inv->orderInput->date_input;
            $inv->date_output = $inv->orderOutput->date_output;
        }
        return $invoiceList;
    }

    public function create(Request $request){

        $rules = [
            'order_output_id' => 'required|exists:order_outputs,id',
            'status' => [
                Rule::in(['UNPAID', 'PAID'])
            ],
            'notes' => ''
        ];

        $res = $this->validateInput($request, $rules);

        // if validation fails, return error.
        if ($res['result'] === 'fail'){
            return $res;
        }

        // if validation successes, input data.
        try {
            DB::beginTransaction();

            $orderOutput = OrderOutput::where('id', $request->order_output_id)->first();
            if (!isset($orderOutput)){
                DB::rollBack();
                return [
                  'result' => 'fail',
                  'message' => "Order output data not found"
                ];
            }

            // create new invoice
            $invoice = new Invoice;

            // calculate hours spent
            $start_date = Carbon::create($orderOutput->orderInput->date_input);
            $end_date = Carbon::create($orderOutput->date_output);
            $total_hours = $start_date->diffInHours($end_date);
            $total_minutes = $start_date->diffInMinutes($end_date);
            $total_hours = $total_minutes % 60 != 0 ? $total_hours + 1 : $total_hours;

            $invoice->customer_id = $orderOutput->orderData->customer_id;
            $invoice->order_input_id = $orderOutput->order_input_id;
            $invoice->order_output_id = $orderOutput->id;
            $invoice->total_minutes = $total_minutes;
            $invoice->amount = $orderOutput->orderData->storage->price_per_hour * $total_hours;
            $invoice->currency = $orderOutput->orderData->storage->currency;
            $invoice->status = isset($request->status) ? $request->status : 'UNPAID';
            $invoice->notes = isset($request->notes) ? $request->notes : '';
            $invoice->save();

            $invoice->code = 'INV-'.str_pad($invoice->id, 8, '0', STR_PAD_LEFT);
            $invoice->save();

            if ($invoice->status === 'PAID'){
                $storage = Storage::where('id', $orderOutput->orderData->storage->id)->first();
                $storage->status = 'VACANT';
                $storage->save();
            }

            DB::commit();

            return [
                'result' => 'success',
                'message' => 'Successfully added new invoice'
            ];
        }
        catch (Exception $ex){
            DB::rollBack();

            return [
                'result' => 'fail',
                'message' => $ex->getMessage()
            ];
        }
    }

    public function update($invoice_id, Request $request){
        $rules = [
            'status' => [
                Rule::in(['UNPAID', 'PAID'])
            ],
            'notes' => '',
        ];

        $res = $this->validateInput($request, $rules);

        // if validation fails, return error.
        if ($res['result'] === 'fail'){
            return $res;
        }

        // if validation successes, input data.
        try {
            DB::beginTransaction();
            $invoice = $this->get($invoice_id);

            if (!isset($invoice)){
                return [
                    'result' => 'fail',
                    'message' => 'Order Output data does not exist.'
                ];
            }

            if (isset($request->status)) {
                $invoice->status = $request->status;
            }

            if (isset($request->notes)) {
                $invoice->notes = $request->notes;
            }

            $invoice->save();

            DB::commit();

            return [
                'result' => 'success',
                'message' => 'Successfully updated invoice.'
            ];
        }
        catch (Exception $ex){
            DB::rollBack();

            return [
                'result' => 'fail',
                'message' => $ex->getMessage()
            ];
        }

    }

    public function delete(Request $request){
        try {
            DB::beginTransaction();
            $invoice = Invoice::where('id', $request->invoice_id)->first();

            if (!isset($invoice)) {
                DB::rollBack();

                return [
                    'result' => 'fail',
                    'message' => 'Data does not exist'
                ];
            }
            $invoice->delete();

            DB::commit();
            return [
                'result' => 'success',
                'message' => 'Successfully deleted invoice.'
            ];

        }
        catch (Exception $ex){
            DB::rollBack();

            return [
                'result' => 'fail',
                'message' => $ex->getMessage()
            ];
        }
    }

    public function validateInput(Request $request, $rules){

        $messages = [
            'order_output_id.required' => "Order Output ID is required",
            'order_output_id.exists' => 'Order Output ID must exist',
            'status.in' => 'Status must be "UNPAID" or "PAID"'
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            $errors = $validator->errors();
            $returnMsg = '';
            foreach ($errors->all() as $message) {
                $returnMsg .= $message . "<br>";
            }
            return [
                'result' => 'fail',
                'message' => $returnMsg
            ];
        }

        return [
            'result' => 'success',
            'message' => 'Success'
        ];
    }

    public function view(){
        return view('invoice.invoice');
    }

    public function viewCreate($order_output_id){
        $invoice = Invoice::where('order_output_id', $order_output_id)->first();

        // if invoice does not exist, can create new invoice.
        if (!isset($invoice)) {
            $orderOutput = OrderOutput::where('id', $order_output_id)->first();

            $start_date = Carbon::create($orderOutput->orderInput->date_input);
            $end_date = Carbon::create($orderOutput->date_output);
            $total_hours = $start_date->diffInHours($end_date);
            $total_minutes = $start_date->diffInMinutes($end_date) % 60;

            $total_amount = $orderOutput->orderData->storage->price_per_hour * ($total_hours + ($total_minutes > 0 ? 1 : 0));

            $total = [
                'hours' => $total_hours,
                'minutes' => $total_minutes,
                'amount' => $total_amount
            ];

            return view('invoice.invoice-create', compact('orderOutput', 'total'));
        }

        return redirect(route('order.output.view'));
    }

    public function viewUpdate($invoice_id){
        $invoice = $this->get($invoice_id);
        return view('invoice.invoice-update', compact('invoice'));
    }
}
