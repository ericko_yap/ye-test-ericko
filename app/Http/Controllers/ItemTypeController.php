<?php

namespace App\Http\Controllers;

use App\Model\ItemType;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use League\Flysystem\Exception;

class ItemTypeController extends Controller
{
    public function get($item_type_id){
        $itemType = ItemType::where('id', $item_type_id)->first();

        return $itemType;
    }

    public function getList(Request $request){
        $itemTypeList = ItemType::all();
        return $itemTypeList;
    }

    public function create(Request $request){

        $rules = [
            'type' => 'required|unique:item_types,type',
            'description' => '',
        ];

        $res = $this->validateInput($request, $rules);

        // if validation fails, return error.
        if ($res['result'] === 'fail'){
            return $res;
        }

        // if validation successes, input data.
        try {
            DB::beginTransaction();
            $itemType = new ItemType;
            $itemType->code = '';
            $itemType->type = $request->type;
            $itemType->description = isset($request->description) ? $request->description : '';

            $itemType->save();

            $itemType->code = 'ITM'.str_pad($itemType->id, 4, '0', STR_PAD_LEFT);
            $itemType->save();

            DB::commit();

            return [
                'result' => 'success',
                'message' => 'Successfully added new item type'
            ];
        }
        catch (Exception $ex){
            DB::rollBack();

            return [
                'result' => 'fail',
                'message' => $ex->getMessage()
            ];
        }
    }

    public function update($item_type_id, Request $request){
        $rules = [
            'description' => '',
        ];

        $res = $this->validateInput($request, $rules);

        // if validation fails, return error.
        if ($res['result'] === 'fail'){
            return $res;
        }

        // if validation successes, input data.
        try {
            DB::beginTransaction();
            $itemType = $this->get($item_type_id);
            $itemType->description = isset($request->description) ? $request->description : '';

            $itemType->save();

            DB::commit();

            return [
                'result' => 'success',
                'message' => 'Successfully updated item type data.'
            ];
        }
        catch (Exception $ex){
            DB::rollBack();

            return [
                'result' => 'fail',
                'message' => $ex->getMessage()
            ];
        }

    }

    public function delete(Request $request){
        try {
            DB::beginTransaction();
            $itemType = ItemType::where('id', $request->item_type_id)->first();

            if (!isset($itemType)) {
                DB::rollBack();

                return [
                    'result' => 'fail',
                    'message' => 'Data does not exist'
                ];
            }

            $itemType->delete();

            DB::commit();
            return [
                'result' => 'success',
                'message' => 'Successfully deleted item type.'
            ];

        }
        catch (Exception $ex){
            DB::rollBack();

            return [
                'result' => 'fail',
                'message' => $ex->getMessage()
            ];
        }
    }

    public function validateInput(Request $request, $rules){

        $messages = [
            'type.required' => 'Type is required and must be filled.',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            $errors = $validator->errors();
            $returnMsg = '';
            foreach ($errors->all() as $message) {
                $returnMsg .= $message . "<br>";
            }
            return [
                'result' => 'fail',
                'message' => $returnMsg
            ];
        }

        return [
            'result' => 'success',
            'message' => 'Success'
        ];
    }

    public function view(){
        return view('item-type.item-type');
    }

    public function viewCreate(){
        return view('item-type.item-type-create');
    }

    public function viewUpdate($item_type_id){
        $itemType = $this->get($item_type_id);
        return view('item-type.item-type-update', compact('itemType'));
    }
}
