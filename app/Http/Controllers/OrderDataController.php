<?php

namespace App\Http\Controllers;

use App\Http\Traits\OrderDataDetailTrait;
use App\Http\Traits\OrderDataTrait;
use Illuminate\Http\Request;

class OrderDataController extends Controller
{
    use OrderDataTrait;

    public function get($order_data_id){
        $orderData = $this->orderDataGet($order_data_id);
        return $orderData;
    }

    public function view($order_data_id, Request $request){
        $orderData = $this->orderDataGet($order_data_id);
        $urlBack = isset($request->ref) ? $request->ref : route('invoice.view');

        return view('order.data.order-data',compact('orderData', 'urlBack'));
    }

    public function viewUpdate($order_data_id){
        $orderData = $this->orderDataGet($order_data_id);
        return view('order.data.order-data-update',compact('orderData'));
    }

    public function detailGet($order_data_detail_id){
        return $this->orderDataDetailGet($order_data_detail_id);
    }

    public function detailGetList($order_data_id){
        $orderDataDetails = $this->orderDataDetailGetList($order_data_id, new Request);
        return $orderDataDetails;
    }

    public function detailUpdate($order_data_detail_id, Request $request){
        return $this->orderDataDetailUpdate($order_data_detail_id, $request);
    }

    public function detailViewUpdate($order_data_detail_id){
        $orderDataDetail = $this->detailGet($order_data_detail_id);
        return view('order.data.detail.order-data-detail-update', compact('orderDataDetail'));
    }

}
