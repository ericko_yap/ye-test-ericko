<?php

namespace App\Http\Controllers;

use App\Http\Traits\OrderDataTrait;
use App\Model\Customer;
use App\Model\ItemType;
use App\Model\OrderData;
use App\Model\OrderInput;
use App\Model\Storage;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use League\Flysystem\Exception;

class OrderInputController extends Controller
{
    use OrderDataTrait;

    public function get($order_input_id){
        $orderInput = OrderInput::where('id', $order_input_id)->first();

        return $orderInput;
    }

    public function getList(Request $request){
        $orderDataIds = OrderData::where('status','RENTED')->select('id')->get();
        $orderInputList = OrderInput::whereIn('order_data_id', $orderDataIds)->get();
        foreach($orderInputList as $orderInput){
            $orderInput->customer_name = $orderInput->orderData->customer->name;
            $orderInput->storage_type = $orderInput->orderData->storage->type;
            $orderInput->status = $orderInput->orderData->status;
        }

        return $orderInputList;
    }

    public function create(Request $request){

        $rules = [
            'notes' => '',
        ];

        $res = $this->validateInput($request, $rules);

        // if validation fails, return error.
        if ($res['result'] === 'fail'){
            return $res;
        }

        // if validation successes, input data.
        try {
            DB::beginTransaction();

            // input order data
            $res = $this->orderDataCreate($request);
            if ($res['result'] === 'fail'){
                DB::rollBack();
                return $res;
            }

            // set storage to "OCCUPIED" if "VACANT"
            $storage = Storage::where('id', $res['data']->storage_id)->first();
            if ($storage->status !== 'VACANT') {
                DB::rollBack();
                return [
                    'result' => 'fail',
                    'message' => 'Storage '.$storage->uid.' is already occupied, please select another storage'
                ];
            }
            $storage->status = 'OCCUPIED';
            $storage->save();

            // create new order input
            $orderInput = new OrderInput;

            $orderInput->order_data_id = $res['data']->id;
            $orderInput->date_input = Carbon::now()->toDateTimeString();
            $orderInput->notes = isset($request->notes) ? $request->notes : '';
            $orderInput->save();

            $orderInput->code = 'ORD-I-'.str_pad($orderInput->id, 8, '0', STR_PAD_LEFT);
            $orderInput->save();

            DB::commit();

            return [
                'result' => 'success',
                'message' => 'Successfully added new order input'
            ];
        }
        catch (Exception $ex){
            DB::rollBack();

            return [
                'result' => 'fail',
                'message' => $ex->getMessage()
            ];
        }
    }

    public function update($order_input_id, Request $request){
        $rules = [
            'status' => [
                'nullable',
                Rule::in(['RENTED','COMPLETED']),
            ],
            'notes' => '',
        ];

        $res = $this->validateInput($request, $rules);

        // if validation fails, return error.
        if ($res['result'] === 'fail'){
            return $res;
        }

        // if validation successes, input data.
        try {
            DB::beginTransaction();
            $orderInput = $this->get($order_input_id);
            if (isset($request->status)) {
                $req = new Request;
                $req->status = $request->status;
                $res = $this->orderDataUpdate($orderInput->order_data_id, $req);

                if ($res['result'] === 'fail'){
                    DB::rollBack();
                    return $res;
                }
            }
            if (isset($request->notes)) {
                $orderInput->notes = $request->notes;
            }

            $orderInput->save();

            DB::commit();

            return [
                'result' => 'success',
                'message' => 'Successfully updated order input data.'
            ];
        }
        catch (Exception $ex){
            DB::rollBack();

            return [
                'result' => 'fail',
                'message' => $ex->getMessage()
            ];
        }

    }

    public function delete(Request $request){
        try {
            DB::beginTransaction();
            $orderInput = OrderInput::where('id', $request->order_input_id)->first();

            if (!isset($orderInput)) {
                DB::rollBack();

                return [
                    'result' => 'fail',
                    'message' => 'Data does not exist'
                ];
            }
            $orderInput->delete();

            DB::commit();
            return [
                'result' => 'success',
                'message' => 'Successfully deleted order input'
            ];

        }
        catch (Exception $ex){
            DB::rollBack();

            return [
                'result' => 'fail',
                'message' => $ex->getMessage()
            ];
        }
    }

    public function validateInput(Request $request, $rules){

        $messages = [
            'status.in' => 'Status does not match RENTED or COMPLETED'
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            $errors = $validator->errors();
            $returnMsg = '';
            foreach ($errors->all() as $message) {
                $returnMsg .= $message . "<br>";
            }
            return [
                'result' => 'fail',
                'message' => $returnMsg
            ];
        }

        return [
            'result' => 'success',
            'message' => 'Success'
        ];
    }

    public function view(){
        return view('order.input.order-input');
    }

    public function viewCreate(){
        $customers = Customer::orderBy('name')->get();
        $storages = Storage::where('status','VACANT')->orderByRaw('FIELD("type","SMALL","MEDIUM","LARGE","OTHER")')->get();
        $itemTypes = ItemType::all();

        return view('order.input.order-input-create', compact('customers', 'storages', 'itemTypes'));
    }

    public function viewUpdate($order_input_id){
        $orderInput = $this->get($order_input_id);
        return view('order.input.order-input-update', compact('orderInput'));
    }
}
