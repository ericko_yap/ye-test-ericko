<?php

namespace App\Http\Controllers;

use App\Http\Traits\OrderDataTrait;
use App\Model\Invoice;
use App\Model\OrderData;
use App\Model\OrderInput;
use App\Model\OrderOutput;
use App\Model\Storage;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Validator;
use League\Flysystem\Exception;

class OrderOutputController extends Controller
{
    use OrderDataTrait;

    public function get($order_output_id){
        $orderOutput = OrderOutput::where('id', $order_output_id)->first();

        return $orderOutput;
    }

    public function getList(Request $request){
        $orderDataIds = OrderData::where('status','COMPLETED')->select('id')->get();
        $orderOutputList = OrderOutput::whereIn('order_data_id', $orderDataIds)->get();
        $res = [];

        foreach($orderOutputList as $orderOutput){
            if (!isset($orderOutput->invoice)){
                $orderOutput->customer_name = $orderOutput->orderData->customer->name;
                $orderOutput->storage_type = $orderOutput->orderData->storage->type;
                $orderOutput->has_invoice = isset($orderOutput->invoice) ? 'true' : 'false';
                $orderOutput->status = $orderOutput->orderData->status;
                $orderOutput->date_input = $orderOutput->orderInput->date_input;
                $res[] = $orderOutput;
            }
        }
        return $res;
    }

    public function create(Request $request){

        $rules = [
            'order_input_id' => 'required|exists:order_inputs,id',
            'notes' => '',
        ];

        $res = $this->validateInput($request, $rules);

        // if validation fails, return error.
        if ($res['result'] === 'fail'){
            return $res;
        }

        // if validation successes, input data.
        try {
            DB::beginTransaction();

            // get order input
            $orderInput = OrderInput::where("id", $request->order_input_id)->first();

            if (!isset($orderInput)){
                DB::rollBack();
                return [
                    'result' => 'fail',
                    'message' => 'Order Input ID not found.'
                ];
            }

            // update order data.
            $reqOrderData = new Request;
            $reqOrderData->status = 'COMPLETED';
            $res = $this->orderDataUpdate($orderInput->order_data_id, $reqOrderData);
            if ($res['result'] === 'fail'){
                DB::rollBack();
                return $res;
            }

            // set storage to "VACANT" if "OCCUPIED
            $storage = Storage::where('id', $res['data']->storage_id)->first();
            if ($storage->status !== 'OCCUPIED') {
                DB::rollBack();
                return [
                    'result' => 'fail',
                    'message' => 'Storage '.$storage->uid.' is vacant, please input correct data.'
                ];
            }
            $storage->status = 'OCCUPIED';
            $storage->save();

            // create new order input
            $orderOutput = new OrderOutput;

            $orderOutput->order_data_id = $res['data']->id;
            $orderOutput->order_input_id = $request->order_input_id;
            $orderOutput->date_output = Carbon::now()->toDateTimeString();
            $orderOutput->notes = isset($request->notes) ? $request->notes : '';
            $orderOutput->save();

            $orderOutput->code = 'ORD-O-'.str_pad($orderOutput->id, 8, '0', STR_PAD_LEFT);
            $orderOutput->save();

            DB::commit();

            return [
                'result' => 'success',
                'message' => 'Successfully added new order output'
            ];
        }
        catch (Exception $ex){
            DB::rollBack();

            return [
                'result' => 'fail',
                'message' => $ex->getMessage()
            ];
        }
    }

    public function update($order_output_id, Request $request){
        $rules = [
            'notes' => '',
        ];

        $res = $this->validateInput($request, $rules);

        // if validation fails, return error.
        if ($res['result'] === 'fail'){
            return $res;
        }

        // if validation successes, input data.
        try {
            DB::beginTransaction();
            $orderOutput = $this->get($order_output_id);

            if (!isset($orderOutput)){
                return [
                    'result' => 'fail',
                    'message' => 'Order Output data does not exist.'
                ];
            }

            if (isset($request->notes)) {
                $orderOutput->notes = $request->notes;
            }

            $orderOutput->save();

            DB::commit();

            return [
                'result' => 'success',
                'message' => 'Successfully updated order output data.'
            ];
        }
        catch (Exception $ex){
            DB::rollBack();

            return [
                'result' => 'fail',
                'message' => $ex->getMessage()
            ];
        }

    }

    public function delete(Request $request){
        try {
            DB::beginTransaction();
            $orderOutput = OrderOutput::where('id', $request->order_output_id)->first();

            if (!isset($orderOutput)) {
                DB::rollBack();

                return [
                    'result' => 'fail',
                    'message' => 'Data does not exist'
                ];
            }
            $orderOutput->delete();

            DB::commit();
            return [
                'result' => 'success',
                'message' => 'Successfully deleted order input'
            ];

        }
        catch (Exception $ex){
            DB::rollBack();

            return [
                'result' => 'fail',
                'message' => $ex->getMessage()
            ];
        }
    }

    public function validateInput(Request $request, $rules){

        $messages = [
            'order_input_id.required' => "Order Input ID is required",
            'order_input_id.exists' => 'Order Input ID must exist'
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            $errors = $validator->errors();
            $returnMsg = '';
            foreach ($errors->all() as $message) {
                $returnMsg .= $message . "<br>";
            }
            return [
                'result' => 'fail',
                'message' => $returnMsg
            ];
        }

        return [
            'result' => 'success',
            'message' => 'Success'
        ];
    }

    public function view(){
        return view('order.output.order-output');
    }

    public function viewCreate($order_input_id){
        $orderOutput = OrderOutput::where('order_input_id', $order_input_id)->first();

        // if order output does not exist.
        if (!isset($orderOutput)) {
            $orderInput = OrderInput::where('id', $order_input_id)->first();
            return view('order.output.order-output-create', compact('orderInput'));
        }

        return redirect(route('order.input.view'));
    }

    public function viewUpdate($order_output_id){
        $orderOutput = $this->get($order_output_id);
        return view('order.output.order-output-update', compact('orderOutput'));
    }
}
