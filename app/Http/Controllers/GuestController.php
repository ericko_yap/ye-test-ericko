<?php

namespace App\Http\Controllers;

use App\Model\OrderInput;
use Illuminate\Http\Request;

class GuestController extends Controller
{
    public function findOrder(Request $request){
        $keyword = $request->keyword;
        $orderInput = OrderInput::where('code', $keyword)->first();
        if (isset($orderInput)) {
            $orderData = $orderInput->orderData;
            return view('guest.find-order', compact('orderInput','orderData','keyword'));
        }
        $errorMessage = 'Order not found!';
        return view("guest.find-order", compact('errorMessage', 'keyword'));
    }

    public function viewFindOrder(){
        return view('guest.find-order');
    }
}
