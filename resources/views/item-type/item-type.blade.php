@extends('layouts.app')

@push('page_css')
<link href="{{ asset('css/jquery.dataTables.min.css') }}" rel="stylesheet">
@endpush

@section('content')
<div class="container">
    <div class="row justify-content-center">
        @include('header')

        <div class="col-md-12" style="margin-top:10px;">
            <div class="card">
                <div class="card-header">
                    <h3>
                        Item Type
                        <div style="float:right">
                            <a href="{{ route('item-type.view.create') }}">
                                <button class="btn btn-outline-primary">
                                        <i class="fa fa-add"></i>
                                        + Create
                                </button>
                            </a>
                        </div>
                    </h3>
                </div>

                <div class="card-body">
                    <table id="tblItemType" class="table table-bordered">
                        <thead>
                        <tr>
                            <th>No</th>
                            <th>Action</th>
                            <th>Code</th>
                            <th>Type</th>
                            <th>Description</th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<input type="hidden" id="urlGetList" value="{{ route('item-type.get-list') }}">
<input type="hidden" id="urlGet" value="{{ url('item-type/view/update') }}">
<input type="hidden" id="urlDelete" value="{{ route('item-type.delete') }}">
<input type="hidden" id="csrfToken" value="{{ csrf_token() }}">
@endsection

@push('page_js')
<script src="{{ asset('js/jquery.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('js/jquery.dataTables.min.js') }}" type="text/javascript" defer></script>
<script>
    var dataTable;

    document.addEventListener('DOMContentLoaded', function(event){
        var urlGetList = document.querySelector('#urlGetList').getAttribute('value');
        dataTable = $('#tblItemType').DataTable(
            {
                processing: true,
                serverSide: true,
                scrollY: 500,
                paging: false,
                searching: false,
                ajax: {
                    url: urlGetList,
                    dataSrc: ''
                },
                columns: [
                    {
                        name: 'No',
                        width: "20px",
                        data: function(row, type, set, meta){
                            return meta.row + 1;
                        }
                    },
                    {
                      name: 'Action',
                      data: function(row){
                          var urlGet = document.querySelector('#urlGet').getAttribute('value') + "/" + row.id;
                          var urlDelete = document.querySelector('#urlDelete').getAttribute('value');
                          var btnViewUpdate = "<a href='"+urlGet+"'><button type='button' class='btn btn-sm btn-light'>View / Update</button></a>";
                          var btnDelete = "<a onclick='confirmDelete(this, " + row.id + ", \"" + urlDelete + "\")'><button type='button' class='btn btn-sm btn-danger'>Delete</button></a>";

                          return btnViewUpdate + "<br>" + btnDelete;
                      }
                    },
                    { data: 'code', name: 'Code' },
                    { data: 'type', name: 'Type' },
                    { data: 'description', name: 'Description' },
                ]
            }
        );
    });

    function confirmDelete(item, id, url){
        var data = new FormData();
        data.append('item_type_id', id);
        data.append('_token', document.querySelector('#csrfToken').getAttribute('value'));

        if (confirm('Are you sure you want to delete this data?')){
            post(url, data, defaultCallbackSuccess, defaultCallbackFail);
        }
    }
</script>
@endpush
