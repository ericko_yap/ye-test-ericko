@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        @include('header')

        <div class="col-md-12" style="margin-top:10px;">
            <div class="col-md-12" style="margin-top:20px;">
                @component('component/button-back',['url' => route('item-type.view')]) @endcomponent
            </div>
            <div class="card" style="margin-top:10px;">
                <div class="card-header">
                    <h3>
                        Create Item Type
                    </h3>
                </div>

                <div class="card-body">
                    <form id="frmItemTypeCreate" action="{{ route('item-type.create') }}" method="post">
                        {{ csrf_field() }}
                        <div class="form-group row">
                            <label for="type" class="col-md-4 col-form-label text-md-right">Type</label>
                            <div class="col-md-6">
                                <input id="type" type="text" name="type" value="" required="required" autofocus="autofocus" class="form-control" placeholder="ex: LIQUID">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="Description" class="col-md-4 col-form-label text-md-right">Description</label>
                            <div class="col-md-6">
                                <textarea class="form-control" id="description" name="description" required="required" row="3" placeholder="ex: All liquids. No exceptions."></textarea>
                            </div>
                        </div>
                        <button id="btnSubmit" type="submit" class="btn btn-primary" style="float:right;">
                            Create
                        </button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('page_js')
<script>
    document.addEventListener("DOMContentLoaded", function(event) {
        initFormById('frmItemTypeCreate', defaultCallbackSuccess, defaultCallbackFail);
    });
</script>
@endpush
