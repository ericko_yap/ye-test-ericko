
<div class="col-md-12">
    <div class="card">
        <div class="card-header">
            <div class="row col-md-12">
                <a href="{{ route('customer.view') }}" class=" btn btn-toolbar" style="text-decoration:none;">Customer</a>
                <a href="{{ route('storage.view') }}" class=" btn btn-toolbar" style="text-decoration:none;">Storage (Gudang)</a>
                <a href="{{ route('item-type.view') }}" class=" btn btn-toolbar" style="text-decoration:none;">Item Type</a>
                <a href="{{ route('order.input.view') }}" class=" btn btn-toolbar" style="text-decoration:none;">Order Input</a>
                <a href="{{ route('order.output.view') }}" class=" btn btn-toolbar" style="text-decoration:none;">Order Output</a>
                <a href="{{ route('invoice.view') }}" class=" btn btn-toolbar" style="text-decoration:none;">Invoice</a>
            </div>
        </div>
    </div>
</div>
