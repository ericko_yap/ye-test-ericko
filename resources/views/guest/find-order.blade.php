@extends('layouts.app')

@push('page_css')
<link href="{{ asset('css/jquery.dataTables.min.css') }}" rel="stylesheet">
@endpush

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12" style="margin-top:10px;">
            <div class="card">
                <div class="card-header">
                    <div class="form-group row">
                        <label for="query" class="col-md-3 col-form-label text-md-right">Find Order</label>
                        <div class="col-md-6">
                            <form id="frmFindOrder" action="{{ route('guest.find-order.find') }}" method="post">
                                <input id="keyword" type="text" name="keyword" value="{{ isset($keyword) ? $keyword : '' }}" required="required" autofocus="autofocus" class="form-control" placeholder="ex: ORD-I-000000001">
                            </form>
                        </div>
                        <button id="btnFindOrder" form="frmFindOrder" type="submit" class="btn btn-primary" style="float:right;">
                            Find
                        </button>
                    </div>
                </div>
            </div>
        </div>

        @if (isset($errorMessage))
            <p style="margin-top:10px;"> {{ $errorMessage }} </p>
        @endif

        @if (isset($orderData))
        <div class="col-md-12" style="margin-top:10px;">
            <div class="card" style="margin-top:10px;">
                <div class="card-header">
                    <h3>
                        Order Data: {{ $orderInput->code }}
                    </h3>
                </div>

                <div class="card-body">
                    <div class="form-group row">
                        <label class="col-md-3 col-form-label text-md-left">Order ID</label>
                        <div class="col-md-3">
                            <span id="code" class="form-control-plaintext">{{ $orderInput->code }}</span>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-md-3 col-form-label text-md-left">Customer</label>
                        <div class="col-md-3">
                            <span id="customer_name" class="form-control-plaintext">{{ $orderData->customer->name }}</span>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-md-3 col-form-label text-md-left">Storage Type</label>
                        <div class="col-md-3">
                            <span id="storage_type" class="form-control-plaintext">{{ $orderData->storage->type }}</span>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-md-3 col-form-label text-md-left">Status</label>
                        <div class="col-md-3">
                            <span id="status" class="form-control-plaintext">{{ $orderData->status }}</span>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-md-3 col-form-label text-md-left">Notes</label>
                        <div class="col-md-3">
                            <span id="notes" class="form-control-plaintext">{{ $orderData->notes }}</span>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <table id="tblOrderDataDetail" class="table table-bordered">
                        <thead>
                        <tr>
                            <th>No</th>
                            <th>Name</th>
                            <th>Type</th>
                            <th>Quantity</th>
                            <th>Unit</th>
                            <th>Notes</th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        @endif
    </div>
</div>
@if (isset($orderData))
<input type="hidden" disabled id="urlGetDetailList" value="{{ url('guest/find-order/detail/get-list/' . $orderData->id) }}">
@endif
@endsection

@if (isset($orderData))
@push('page_js')
<script src="{{ asset('js/jquery.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('js/jquery.dataTables.min.js') }}" type="text/javascript" defer></script>
<script>
    var dataTable;

    document.addEventListener('DOMContentLoaded', function(event){
        var urlGetDetailList = document.querySelector('#urlGetDetailList').getAttribute('value');
        dataTable = $('#tblOrderDataDetail').DataTable(
            {
                processing: true,
                serverSide: true,
                scrollY: 500,
                paging: false,
                searching: false,
                ajax: {
                    url: urlGetDetailList,
                    dataSrc: ''
                },
                columns: [
                    {
                        name: 'No',
                        width: "20px",
                        data: function(row, type, set, meta){
                            return meta.row + 1;
                        }
                    },
                    { data: 'item_name', name: 'Name' },
                    { data: 'item_type_name', name: 'Type' },
                    { data: 'quantity', name: 'Quantity' },
                    { data: 'unit', name: 'Unit' },
                    { data: 'notes', name: 'Notes' }
                ]
            }
        );
    });
</script>
@endpush
@endif
