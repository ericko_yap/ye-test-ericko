@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        @include('header')

        <div class="col-md-12" style="margin-top:10px;">
            <div class="col-md-12" style="margin-top:20px;">
                @component('component/button-back',['url' => route('storage.view')]) @endcomponent
            </div>
            <div class="card" style="margin-top:10px;">
                <div class="card-header">
                    <h3>
                        Create Storage
                    </h3>
                </div>

                <div class="card-body">
                    <form id="frmStorageCreate" action="{{ route('storage.create') }}" method="post">
                        {{ csrf_field() }}
                        <div class="form-group row">
                            <label for="type" class="col-md-4 col-form-label text-md-right">Type</label>
                            <div class="col-md-6">
                                <select id="type" name="type" required="required" value="" class="form-control">
                                    <option value="" selected>Select Type...</option>
                                    <option value="SMALL">SMALL</option>
                                    <option value="MEDIUM">MEDIUM</option>
                                    <option value="LARGE">LARGE</option>
                                    <option value="OTHER">OTHER</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="capacity" class="col-md-4 col-form-label text-md-right">Capacity</label>
                            <div class="col-md-6">
                                <input id="capacity" type="text" name="capacity" value="" required="required" autocomplete="capacity" autofocus="autofocus" class="form-control" placeholder="ex: 1200L / 20x50">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="description" class="col-md-4 col-form-label text-md-right">Description</label>
                            <div class="col-md-6">
                                <textarea class="form-control" id="description" name="description" required="required" row="3" placeholder="ex: This is a small room."></textarea>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="location" class="col-md-4 col-form-label text-md-right">Location</label>
                            <div class="col-md-6">
                                <textarea class="form-control" id="location" name="location" required="required" row="3" placeholder="ex: At under the bridge."></textarea>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="price_per_hour" class="col-md-4 col-form-label text-md-right">Price Per Hour</label>
                            <div class="col-md-6">
                                <input id="price_per_hour" type="number" min="1" name="price_per_hour" value="" required="required" autocomplete="price_per_hour" autofocus="autofocus" class="form-control" placeholder="ex: 10000">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="currency" class="col-md-4 col-form-label text-md-right">Currency</label>
                            <div class="col-md-6">
                                <input id="currency" type="text" min="1" name="currency" value="" required="required" autocomplete="currency" autofocus="autofocus" class="form-control" placeholder="ex: IDR">
                            </div>
                        </div>
                        <button id="btnSubmit" type="submit" class="btn btn-primary" style="float:right;">
                            Create
                        </button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('page_js')
<script>
    document.addEventListener("DOMContentLoaded", function(event) {
        initFormById('frmStorageCreate', defaultCallbackSuccess, defaultCallbackFail);
    });
</script>
@endpush
