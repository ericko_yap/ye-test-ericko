@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        @include('header')

        <div class="col-md-12" style="margin-top:10px;">
            <div class="col-md-12" style="margin-top:20px;">
                @component('component/button-back',['url' => route('storage.view')]) @endcomponent
            </div>
            <div class="card" style="margin-top:10px;">
                <div class="card-header">
                    <h3>
                        Update Storage
                    </h3>
                </div>

                <div class="card-body">
                    <form id="frmStorageUpdate" action="{{ route('storage.update',['storage_id' => $storage->id]) }}" method="post">
                        {{ csrf_field() }}
                        <div class="form-group row">
                            <label for="type" class="col-md-4 col-form-label text-md-right">Type</label>
                            <div class="col-md-6">
                                <select id="type" name="type" required="required" class="form-control">
                                    <option value="">Select Type...</option>
                                    <option value="SMALL" {{ (isset($storage) && $storage->type === 'SMALL' ? 'selected' : '') }}>SMALL</option>
                                    <option value="MEDIUM" {{ (isset($storage) && $storage->type === 'MEDIUM' ? 'selected' : '') }}>MEDIUM</option>
                                    <option value="LARGE" {{ (isset($storage) && $storage->type === 'LARGE' ? 'selected' : '') }}>LARGE</option>
                                    <option value="OTHER" {{ (isset($storage) && $storage->type === 'OTHER' ? 'selected' : '') }}>OTHER</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="capacity" class="col-md-4 col-form-label text-md-right">Capacity</label>
                            <div class="col-md-6">
                                <input id="capacity" type="text" name="capacity" required="required" autocomplete="capacity" autofocus="autofocus" class="form-control" placeholder="ex: 1200L / 20x50"
                                       value="{{ isset($storage) ? $storage->capacity : '' }}" >
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="description" class="col-md-4 col-form-label text-md-right">Description</label>
                            <div class="col-md-6">
                                <textarea class="form-control" id="description" name="description" required="required" row="3" placeholder="ex: This is a small room.">{{ isset($storage) ? $storage->description : '' }}</textarea>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="location" class="col-md-4 col-form-label text-md-right">Location</label>
                            <div class="col-md-6">
                                <textarea class="form-control" id="location" name="location" required="required" row="3" placeholder="ex: At under the bridge.">{{ isset($storage) ? $storage->location : '' }}</textarea>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="price_per_hour" class="col-md-4 col-form-label text-md-right">Price Per Hour</label>
                            <div class="col-md-6">
                                <input id="price_per_hour" type="number" min="1" name="price_per_hour" required="required" autocomplete="price_per_hour" autofocus="autofocus" class="form-control" placeholder="ex: 10000"
                                       value="{{ isset($storage) ? $storage->price_per_hour : '' }}" >
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="currency" class="col-md-4 col-form-label text-md-right">Currency</label>
                            <div class="col-md-6">
                                <input id="currency" type="text" min="1" name="currency" required="required" autocomplete="currency" autofocus="autofocus" class="form-control" placeholder="ex: IDR"
                                       value="{{ isset($storage) ? $storage->currency : '' }}" >
                            </div>
                        </div>
                        <button id="btnSubmit" type="submit" class="btn btn-primary" style="float:right;">
                            Update
                        </button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('page_js')
<script>
    document.addEventListener("DOMContentLoaded", function(event) {
        initFormById('frmStorageUpdate', defaultCallbackSuccess, defaultCallbackFail);
    });
</script>
@endpush
