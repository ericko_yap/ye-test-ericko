@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        @include('header')

        <div class="col-md-12" style="margin-top:10px;">
            <div class="col-md-12" style="margin-top:20px;">
                @component('component/button-back',['url' => route('order.output.view')]) @endcomponent
            </div>
            <div class="card" style="margin-top:10px;">
                <div class="card-header">
                    <h3>
                        Create Invoice
                    </h3>
                </div>

                <div class="card-body">
                    <form id="frmInvoiceCreate" action="{{ route('invoice.create') }}" method="post">
                        {{ csrf_field() }}
                        <input type="hidden" name="order_output_id" value="{{ $orderOutput->id }}">
                        <div class="form-group row">
                            <label class="col-md-4 col-form-label text-md-right">Order Input ID</label>
                            <div class="col-md-6">
                                <span id="input_code" class="form-control-plaintext">{{ $orderOutput->orderInput->code }}</span>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-4 col-form-label text-md-right">Order Output ID</label>
                            <div class="col-md-6">
                                <span id="output_code" class="form-control-plaintext">{{ $orderOutput->code }}</span>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-4 col-form-label text-md-right">Customer</label>
                            <div class="col-md-6">
                                <span id="customer_name" class="form-control-plaintext">{{ $orderOutput->orderData->customer->name }}</span>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-4 col-form-label text-md-right">Storage Type</label>
                            <div class="col-md-6">
                                <span id="storage_type" class="form-control-plaintext">{{ $orderOutput->orderData->storage->type }}</span>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-4 col-form-label text-md-right">Storage Price / Hour</label>
                            <div class="col-md-6">
                                <span id="storage_price_per_hour" class="form-control-plaintext">{{ $orderOutput->orderData->storage->currency . ' ' . $orderOutput->orderData->storage->price_per_hour }}</span>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-4 col-form-label text-md-right">Start Date</label>
                            <div class="col-md-6">
                                <span id="date_input" class="form-control-plaintext">{{ $orderOutput->orderInput->date_input }}</span>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-4 col-form-label text-md-right">End Date</label>
                            <div class="col-md-6">
                                <span id="date_output" class="form-control-plaintext">{{ $orderOutput->date_output }}</span>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-4 col-form-label text-md-right">Time Spent</label>
                            <div class="col-md-6">
                                <span id="time_spent" class="form-control-plaintext">{{ $total['hours'] . ' hours ' . $total['minutes'] . ' minutes'  }}</span>
                            </div>
                        </div>
                        <div class="form-group row" style="font-weight:bold;">
                            <label class="col-md-4 col-form-label text-md-right" >Total Amount</label>
                            <div class="col-md-6">
                                <span class="form-control-plaintext"> {{ $orderOutput->orderData->storage->currency . ' ' . $total['amount']  }} </span>
                            </div>
                        </div>


                        <div class="form-group row">
                            <label for="status" class="col-md-4 col-form-label text-md-right">Status</label>
                            <div class="col-md-6">
                                <select id="status" name="status" required="required" class="form-control">
                                    <option value="">Select status...</option>
                                    <option value="UNPAID" >UNPAID</option>
                                    <option value="PAID" >PAID</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="notes" class="col-md-4 col-form-label text-md-right">Notes</label>
                            <div class="col-md-6">
                                <textarea class="form-control" id="notes" name="notes" required="required" row="3" placeholder="ex: Important order, do not mess up."></textarea>
                            </div>
                        </div>
                        <button id="btnSubmit" type="submit" class="btn btn-primary" style="float:right;">
                            Create
                        </button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<input type="hidden" disabled id="urlViewInvoice" value="{{ route('invoice.view') }}">
@endsection

@push('page_js')
<script>
    document.addEventListener("DOMContentLoaded", function(event) {
        initFormById('frmInvoiceCreate', function(responseText){
            var res = JSON.parse(responseText);
            console.log(res);
            if (res.result === 'success') {
                alert(res.message);
                window.location = document.querySelector(('#urlViewInvoice')).getAttribute('value');
            }
            else if (res.result === 'fail'){
                defaultCallbackFail(responseText);
            }
        }, defaultCallbackFail);
    });
</script>
@endpush
