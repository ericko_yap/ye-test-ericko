@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        @include('header')

        <div class="col-md-12" style="margin-top:10px;">
            <div class="col-md-12" style="margin-top:20px;">
                @component('component/button-back',['url' => route('invoice.view')]) @endcomponent
            </div>
            <div class="card" style="margin-top:10px;">
                <div class="card-header">
                    <h3>
                        Update Invoice: {{ $invoice->code }}
                    </h3>
                </div>

                <div class="card-body">
                    <form id="frmInvoiceUpdate" action="{{ route('invoice.update',['invoice_id' => $invoice->id]) }}" method="post">
                        {{ csrf_field() }}
                        <div class="form-group row">
                            <label for="status" class="col-md-4 col-form-label text-md-right">Status</label>
                            <div class="col-md-6">
                                <select id="status" name="status" required="required" class="form-control">
                                    <option value="">Select status...</option>
                                    <option value="UNPAID" {{ (isset($invoice) && $invoice->status === 'UNPAID' ? 'selected' : '') }}>UNPAID</option>
                                    <option value="PAID" {{ (isset($invoice) && $invoice->status === 'PAID' ? 'selected' : '') }}>PAID</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="notes" class="col-md-4 col-form-label text-md-right">Notes</label>
                            <div class="col-md-6">
                                <textarea class="form-control" id="notes" name="notes" required="required" row="3" placeholder="ex: Important order, do not mess up.">{{ isset($invoice) ? $invoice->notes : '' }}</textarea>
                            </div>
                        </div>
                        <button id="btnSubmit" type="submit" class="btn btn-primary" style="float:right;">
                            Update
                        </button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('page_js')
<script>
    document.addEventListener("DOMContentLoaded", function(event) {
        initFormById('frmInvoiceUpdate', defaultCallbackSuccess, defaultCallbackFail);
    });
</script>
@endpush
