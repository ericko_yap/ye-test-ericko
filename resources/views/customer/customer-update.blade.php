@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        @include('header')

        <div class="col-md-12" style="margin-top:10px;">
            <div class="col-md-12" style="margin-top:20px;">
                @component('component/button-back',['url' => route('customer.view')]) @endcomponent
            </div>
            <div class="card" style="margin-top:10px;">
                <div class="card-header">
                    <h3>
                        Update Customer
                    </h3>
                </div>

                <div class="card-body">
                    <form id="frmCustomerUpdate" action="{{ route('customer.update',['customer_id' => $customer->id]) }}" method="post">
                        {{ csrf_field() }}
                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">Full Name</label>
                            <div class="col-md-6">
                                <input id="name" type="text" name="name" required="required" autofocus="autofocus" class="form-control"
                                       placeholder="ex: Ericko Yap"
                                       value="{{ isset($customer) ? $customer->name : '' }}">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">E-Mail Address</label>
                            <div class="col-md-6">
                                <input id="email" type="email" name="email" required="required" autocomplete="email" autofocus="autofocus" class="form-control"
                                       placeholder="ex: a@b.com"
                                       value="{{ isset($customer) ? $customer->email : '' }}">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="phone_number" class="col-md-4 col-form-label text-md-right">Phone Number</label>
                            <div class="col-md-6">
                                <input id="phone_number" type="tel" name="phone_number" required="required" autocomplete="phone_number" autofocus="autofocus" class="form-control" placeholder="ex: +6281807788992"
                                       value="{{ isset($customer) ? $customer->phone_number : '' }}">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="address" class="col-md-4 col-form-label text-md-right">Address</label>
                            <div class="col-md-6">
                                <textarea class="form-control" id="address" name="address" required="required" row="3"
                                          placeholder="ex: Trimezia 3 no. 15, Gading Serpong, Tangerang">{{ isset($customer) ? $customer->address : '' }}</textarea>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="id_number" class="col-md-4 col-form-label text-md-right">Citizen ID</label>
                            <div class="col-md-6">
                                <input id="id_number" type="text" name="id_number" required="required" autocomplete="id_number" autofocus="autofocus" class="form-control"
                                       placeholder="ex: 123456789098765"
                                       value="{{ isset($customer) ? $customer->id_number : '' }}">
                            </div>
                        </div>
                        <button id="btnSubmit" type="submit" class="btn btn-primary" style="float:right;">
                            Update
                        </button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('page_js')
<script>
    document.addEventListener("DOMContentLoaded", function(event) {
        initFormById('frmCustomerUpdate', defaultCallbackSuccess, defaultCallbackFail);
    });
</script>
@endpush
