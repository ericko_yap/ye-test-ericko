@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        @include('header')

        <div class="col-md-12" style="margin-top:10px;">
            <div class="col-md-12" style="margin-top:20px;">
                @component('component/button-back',['url' => route('order.input.view')]) @endcomponent
            </div>
            <div class="card" style="margin-top:10px;">
                <div class="card-header">
                    <h3>
                        Create Order Input
                    </h3>
                </div>

                <div class="card-body">
                    <form id="frmOrderInputCreate" action="{{ route('order.input.create') }}" method="post">
                        {{ csrf_field() }}
                        <label class="col-md-4 col-form-label text-md-right"><h5 style="font-weight:bold;">Order Data</h5></label>
                        <div class="form-group row">
                            <label for="customer_id" class="col-md-4 col-form-label text-md-right">Customer</label>
                            <div class="col-md-6">
                                <select id="customer_id" name="customer_id" required="required" class="form-control">
                                    <option value="">Select Customer...</option>
                                    @foreach($customers as $customer)
                                        <option value="{{ $customer->id }}">{{ $customer->name . ' - ' . $customer->email }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="storage_id" class="col-md-4 col-form-label text-md-right">Storage</label>
                            <div class="col-md-6">
                                <select id="storage_id" name="storage_id" required="required" class="form-control">
                                    <option value="">Select Storage...</option>
                                    @foreach($storages as $storage)
                                        <option value="{{ $storage->id }}">{{ $storage->storage_uid . ' - ' . $storage->type . ' - ' . $storage->capacity . ' - ' . $storage->currency . ' ' . $storage->price_per_hour }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="notes" class="col-md-4 col-form-label text-md-right">Notes</label>
                            <div class="col-md-6">
                                <textarea class="form-control" id="notes" name="notes" required="required" row="3" placeholder="ex: This new order is interesting"></textarea>
                            </div>
                        </div>

                        <!-- Add Order Data Detail -->
                        <div id="detailItems">
                            <div class="orderDataDetail">
                                <label class="col-md-4 col-form-label text-md-right"><h5 style="font-weight:bold;">Detail Item</h5></label>
                                <div class="form-group row">
                                    <label for="detail_item_name" class="col-md-4 col-form-label text-md-right">Item Name</label>
                                    <div class="col-md-6">
                                        <input id="detail_item_name" type="text" name="details[0][item_name]" value="" required="required" autofocus="autofocus" class="form-control" placeholder="ex: Kardus Rokok">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="detail_item_type_id" class="col-md-4 col-form-label text-md-right">Item Type</label>
                                    <div class="col-md-6">
                                        <select id="detail_item_type_id" name="details[0][item_type_id]" required="required" class="form-control">
                                            <option value="">Select Item Type...</option>
                                            @foreach($itemTypes as $itemType)
                                                <option value="{{ $itemType->id }}">{{ $itemType->code . ' - ' . $itemType->type }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="detail_quantity" class="col-md-4 col-form-label text-md-right">Quantity</label>
                                    <div class="col-md-6">
                                        <input id="detail_quantity" type="number" min="1" name="details[0][quantity]" value="" required="required" autocomplete="quantity" autofocus="autofocus" class="form-control" placeholder="ex: 5">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="detail_item_unit" class="col-md-4 col-form-label text-md-right">Unit</label>
                                    <div class="col-md-6">
                                        <input id="detail_item_unit" type="text" name="details[0][unit]" value="" required="required" autofocus="autofocus" class="form-control" placeholder="ex: Box">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="detail_notes" class="col-md-4 col-form-label text-md-right">Notes</label>
                                    <div class="col-md-6">
                                        <input id="detail_notes" type="text" name="details[0][notes]" value="" required="required" autofocus="autofocus" class="form-control" placeholder="ex: Ini barang luar biasa.">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-4"></div>
                            <div class="col-md-6">
                                <button id="btnAddDetailItem" type="button" class="btn btn-outline-primary btn-block form-control">
                                    + Add Detail Item
                                </button>
                            </div>
                        </div>
                        <button id="btnSubmit" type="submit" class="btn btn-primary" style="float:right;">
                            Create
                        </button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<input type="hidden" disabled id="itemTypes" value="{{ json_encode($itemTypes) }}">
@endsection

@push('page_js')
<script>
    var idx = 0;
    var htmlDetailItem = `
        <div class="orderDataDetail_{idx}">

            <label class="col-md-4 col-form-label text-md-right">
                <h5 style="font-weight:bold;">
                    Detail Item
                    <button id="btnAddDetailItem" type="button" class="btn btn-sm btn-outline-danger" onclick="removeHtmlDetailItem('orderDataDetail_{idx}')">
                        X
                    </button>
                </h5>
            </label>

            <div class="form-group row">
                <label for="detail_item_name" class="col-md-4 col-form-label text-md-right">Item Name</label>
                <div class="col-md-6">
                    <input id="detail_item_name" type="text" name="details[{idx}][item_name]" value="" required="required" autofocus="autofocus" class="form-control" placeholder="ex: Kardus Rokok">
                </div>
            </div>
            <div class="form-group row">
                <label for="detail_item_type_id" class="col-md-4 col-form-label text-md-right">Item Type</label>
                <div class="col-md-6">
                    <select id="detail_item_type_id" name="details[{idx}][item_type_id]" required="required" class="form-control">
                        <option value="">Select Item Type...</option>
                        {itemTypeOptions}
                    </select>
                </div>
            </div>
            <div class="form-group row">
                <label for="detail_quantity" class="col-md-4 col-form-label text-md-right">Quantity</label>
                <div class="col-md-6">
                    <input id="detail_quantity" type="number" min="1" name="details[{idx}][quantity]" value="" required="required" autocomplete="quantity" autofocus="autofocus" class="form-control" placeholder="ex: 5">
                </div>
            </div>
            <div class="form-group row">
                <label for="detail_item_unit" class="col-md-4 col-form-label text-md-right">Unit</label>
                <div class="col-md-6">
                    <input id="detail_item_unit" type="text" name="details[{idx}][unit]" value="" required="required" autofocus="autofocus" class="form-control" placeholder="ex: Box">
                </div>
            </div>
            <div class="form-group row">
                <label for="detail_notes" class="col-md-4 col-form-label text-md-right">Notes</label>
                <div class="col-md-6">
                    <input id="detail_notes" type="text" name="details[{idx}][notes]" value="" required="required" autofocus="autofocus" class="form-control" placeholder="ex: Ini barang luar biasa.">
                </div>
            </div>
        </div>
    `;

    function initHtmlDetailItemTypeOptions(){
        var itemTypes = JSON.parse(document.querySelector('#itemTypes').getAttribute('value'));
        var itemTypeOptions = '';
        for (var a = 0; a < itemTypes.length; a++){
            var itemType = itemTypes[a];
            var opt = '<option value="' + itemType.id + '">' + itemType.code + ' - ' + itemType.type + "</option>";
            itemTypeOptions += opt;
        }

        htmlDetailItem = htmlDetailItem.replace('{itemTypeOptions}', itemTypeOptions);
    }

    function getHtmlDetailItem(idx){
        var newHtmlDetailItem = htmlDetailItem;
        return newHtmlDetailItem.replace(/{idx}/g, idx);
    }

    function removeHtmlDetailItem(selector){
        var obj = document.querySelector('.'+selector);
        obj.parentNode.removeChild(obj);
    }

    document.addEventListener("DOMContentLoaded", function(event) {
        initFormById('frmOrderInputCreate', defaultCallbackSuccess, defaultCallbackFail);
        initHtmlDetailItemTypeOptions();

        // dynamic add detail item slot
        var btnAddDetailItem = document.querySelector('#btnAddDetailItem');
        btnAddDetailItem.addEventListener('click', function(event){
            var divDetailItems = document.querySelector('#detailItems');
            var item = getHtmlDetailItem(++idx);
            divDetailItems.insertAdjacentHTML( "beforeend", item);
        });
    });
</script>
@endpush
