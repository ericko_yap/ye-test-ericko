@extends('layouts.app')

@push('page_css')
<link href="{{ asset('css/jquery.dataTables.min.css') }}" rel="stylesheet">
@endpush

@section('content')
<div class="container">
    <div class="row justify-content-center">
        @include('header')

        <div class="col-md-12" style="margin-top:10px;">
            <div class="col-md-12" style="margin-top:20px;">
                @component('component/button-back',['url' => (isset($urlBack) ? $urlBack : route('invoice.view'))]) @endcomponent
            </div>
            <div class="card" style="margin-top:10px;">
                <div class="card-header">
                    <h3>
                        Order Data: {{ $orderData->code }}
                    </h3>
                </div>

                <div class="card-body">
                    <div class="form-group row">
                        <label class="col-md-3 col-form-label text-md-left">Order Data ID</label>
                        <div class="col-md-3">
                            <span id="code" class="form-control-plaintext">{{ $orderData->code }}</span>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-md-3 col-form-label text-md-left">Customer</label>
                        <div class="col-md-3">
                            <span id="customer_name" class="form-control-plaintext">{{ $orderData->customer->name }}</span>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-md-3 col-form-label text-md-left">Storage Type</label>
                        <div class="col-md-3">
                            <span id="storage_type" class="form-control-plaintext">{{ $orderData->storage->type }}</span>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-md-3 col-form-label text-md-left">Status</label>
                        <div class="col-md-3">
                            <span id="status" class="form-control-plaintext">{{ $orderData->status }}</span>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-md-3 col-form-label text-md-left">Notes</label>
                        <div class="col-md-3">
                            <span id="notes" class="form-control-plaintext">{{ $orderData->notes }}</span>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <table id="tblOrderDataDetail" class="table table-bordered">
                        <thead>
                        <tr>
                            <th>No</th>
<!--                            <th>Action</th>-->
                            <th>Name</th>
                            <th>Type</th>
                            <th>Quantity</th>
                            <th>Unit</th>
                            <th>Notes</th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<input type="hidden" disabled id="urlGetDetailList" value="{{ url('order/data/detail/get-list/' . $orderData->id) }}">
<input type="hidden" disabled id="urlGetDetail" value="{{ url('order/data/detail/view/update') }}">
<input type="hidden" disabled id="csrfToken" value="{{ csrf_token() }}">
@endsection

@push('page_js')
<script src="{{ asset('js/jquery.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('js/jquery.dataTables.min.js') }}" type="text/javascript" defer></script>
<script>
    var dataTable;

    document.addEventListener('DOMContentLoaded', function(event){
        var urlGetDetailList = document.querySelector('#urlGetDetailList').getAttribute('value');
        dataTable = $('#tblOrderDataDetail').DataTable(
            {
                processing: true,
                serverSide: true,
                scrollY: 500,
                paging: false,
                searching: false,
                ajax: {
                    url: urlGetDetailList,
                    dataSrc: ''
                },
                columns: [
                    {
                        name: 'No',
                        width: "20px",
                        data: function(row, type, set, meta){
                            return meta.row + 1;
                        }
                    },
//                    {
//                      name: 'Action',
//                      data: function(row){
//                          var urlGetDetail = document.querySelector('#urlGetDetail').getAttribute('value') + "/" + row.id;
//                          var btnViewUpdate = "<a href='"+urlGetDetail+"'><button type='button' class='btn btn-sm btn-light'>View / Update</button></a>";
//
//                          return btnViewUpdate;
//                      }
//                    },
                    { data: 'item_name', name: 'Name' },
                    { data: 'item_type_name', name: 'Type' },
                    { data: 'quantity', name: 'Quantity' },
                    { data: 'unit', name: 'Unit' },
                    { data: 'notes', name: 'Notes' }
                ]
            }
        );
    });

    function confirmDelete(item, id, url){
        var data = new FormData();
        data.append('order_data_detail_id', id);
        data.append('_token', document.querySelector('#csrfToken').getAttribute('value'));

        if (confirm('Are you sure you want to delete this data?')){
            post(url, data, defaultCallbackSuccess, defaultCallbackFail);
        }
    }
</script>
@endpush
