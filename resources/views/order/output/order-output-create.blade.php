@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        @include('header')

        <div class="col-md-12" style="margin-top:10px;">
            <div class="col-md-12" style="margin-top:20px;">
                @component('component/button-back',['url' => route('order.input.view')]) @endcomponent
            </div>
            <div class="card" style="margin-top:10px;">
                <div class="card-header">
                    <h3>
                        Create Order Output
                    </h3>
                </div>

                <div class="card-body">
                    <form id="frmOrderOutputCreate" action="{{ route('order.output.create') }}" method="post">
                        {{ csrf_field() }}
                        <input type="hidden" name="order_input_id" value="{{ $orderInput->id }}">
                        <div class="form-group row">
                            <label class="col-md-4 col-form-label text-md-right">Order Input ID</label>
                            <div class="col-md-6">
                                <span id="code" class="form-control-plaintext">{{ $orderInput->code }}</span>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-4 col-form-label text-md-right">Customer</label>
                            <div class="col-md-6">
                                <span id="customer_name" class="form-control-plaintext">{{ $orderInput->orderData->customer->name }}</span>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-4 col-form-label text-md-right">Storage Type</label>
                            <div class="col-md-6">
                                <span id="storage_type" class="form-control-plaintext">{{ $orderInput->orderData->storage->type }}</span>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-4 col-form-label text-md-right">Start Date</label>
                            <div class="col-md-6">
                                <span id="date_start" class="form-control-plaintext">{{ $orderInput->date_input }}</span>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="Notes" class="col-md-4 col-form-label text-md-right">Notes</label>
                            <div class="col-md-6">
                                <textarea class="form-control" id="notes" name="notes" required="required" row="3" placeholder="ex: Finally this item is out."></textarea>
                            </div>
                        </div>
                        <button id="btnSubmit" type="submit" class="btn btn-primary" style="float:right;">
                            Create
                        </button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<input type="hidden" disabled id="urlViewOrderOutput" value="{{ route('order.output.view') }}">
@endsection

@push('page_js')
<script>
    document.addEventListener("DOMContentLoaded", function(event) {
        initFormById('frmOrderOutputCreate', function(responseText){
            var res = JSON.parse(responseText);
            console.log(res);
            if (res.result === 'success') {
                alert(res.message);
                window.location = document.querySelector(('#urlViewOrderOutput')).getAttribute('value');
            }
            else if (res.result === 'fail'){
                defaultCallbackFail(responseText);
            }
        }, defaultCallbackFail);
    });
</script>
@endpush
