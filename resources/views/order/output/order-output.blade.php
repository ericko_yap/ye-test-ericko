@extends('layouts.app')

@push('page_css')
<link href="{{ asset('css/jquery.dataTables.min.css') }}" rel="stylesheet">
@endpush

@section('content')
<div class="container">
    <div class="row justify-content-center">
        @include('header')

        <div class="col-md-12" style="margin-top:10px;">
            <div class="card">
                <div class="card-header">
                    <h3>
                        Order Output
                    </h3>
                </div>

                <div class="card-body">
                    <table id="tblOrderOutput" class="table table-bordered">
                        <thead>
                        <tr>
                            <th>No</th>
                            <th>Action</th>
                            <th>Code</th>
                            <th>Customer</th>
                            <th>Storage Type</th>
                            <th>Status</th>
                            <th>Order Data</th>
                            <th>Start Date</th>
                            <th>End Date</th>
                            <th>Notes</th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<input type="hidden" disabled id="urlGetList" value="{{ route('order.output.get-list') }}">
<input type="hidden" disabled id="urlGet" value="{{ url('order/output/view/update') }}">
<input type="hidden" disabled id="urlViewInvoiceCreate" value="{{ url('invoice/view/create') }}">
<input type="hidden" disabled id="urlViewOrderData" value="{{ url('order/data/view') }}">
<input type="hidden" disabled id="urlViewOrderDataBackRef" value="{{ route('order.output.view') }}">
<input type="hidden" disabled id="csrfToken" value="{{ csrf_token() }}">
@endsection

@push('page_js')
<script src="{{ asset('js/jquery.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('js/jquery.dataTables.min.js') }}" type="text/javascript" defer></script>
<script>
    var dataTable;

    document.addEventListener('DOMContentLoaded', function(event){
        var urlGetList = document.querySelector('#urlGetList').getAttribute('value');
        dataTable = $('#tblOrderOutput').DataTable(
            {
                processing: true,
                serverSide: true,
                scrollY: 500,
                paging: false,
                searching: false,
                ajax: {
                    url: urlGetList,
                    dataSrc: ''
                },
                columns: [
                    {
                        name: 'No',
                        width: "20px",
                        data: function(row, type, set, meta){
                            return meta.row + 1;
                        }
                    },
                    {
                      name: 'Action',
                      data: function(row){
                          var urlGet = document.querySelector('#urlGet').getAttribute('value') + "/" + row.id;
                          var urlViewInvoiceCreate = document.querySelector('#urlViewInvoiceCreate').getAttribute('value') + "/" + row.id;
                          var btnViewUpdate = "<a href='"+urlGet+"'><button type='button' class='btn btn-sm btn-light'>View / Update</button></a>";
                          var btnViewInvoiceCreate = "<a href='"+urlViewInvoiceCreate+"'><button type='button' class='btn btn-sm btn-outline-primary'>Create Invoice</button></a>";

                          return btnViewUpdate + (row.has_invoice === 'false' ? "<br>" + btnViewInvoiceCreate : '');
                      }
                    },
                    { data: 'code', name: 'Code' },
                    { data: 'customer_name', name: 'Customer' },
                    { data: 'storage_type', name: 'Storage Type' },
                    { data: 'status', name: 'Status' },
                    {
                        name: 'Order Data',
                        data: function(row){
                            var urlViewOrderData = document.querySelector('#urlViewOrderData').getAttribute('value') + "/" + row.order_data_id;
                            var urlViewOrderBackRef = document.querySelector('#urlViewOrderDataBackRef').getAttribute('value');
                            urlViewOrderData += '?ref=' + urlViewOrderBackRef;
                            return "<a href='"+urlViewOrderData+"'><button  type='button' class='btn btn-sm btn-outline-primary'>View Order Data</button></a>";
                        }
                    },
                    { data: 'date_input', name: 'Start Date' },
                    { data: 'date_output', name: 'End Date' },
                    { data: 'notes', name: 'Notes' }
                ]
            }
        );
    });

    function confirmDelete(item, id, url){
        var data = new FormData();
        data.append('order_output_id', id);
        data.append('_token', document.querySelector('#csrfToken').getAttribute('value'));

        if (confirm('Are you sure you want to delete this data?')){
            post(url, data, defaultCallbackSuccess, defaultCallbackFail);
        }
    }
</script>
@endpush
