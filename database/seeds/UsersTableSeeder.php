<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('users')->delete();
        
        \DB::table('users')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'erickoyap',
                'email' => 'the.ericko.mail@gmail.com',
                'email_verified_at' => NULL,
                'password' => '$2y$10$U0JVK/qEcIf3YrTcfIL62OoYnRIEFWeGjrWDDZCkyio10z0tTkM5S',
                'remember_token' => NULL,
                'created_at' => '2019-05-13 09:02:38',
                'updated_at' => '2019-05-13 09:02:38',
            ),
            1 => 
            array (
                'id' => 2,
                'name' => 'admin2',
                'email' => 'admin2@youexpress.com',
                'email_verified_at' => NULL,
                'password' => '$2y$10$zOS5rcojc1wrnIZT9H6K2OO4/wBO0SghynB8xrklIRjoWyACO8bnS',
                'remember_token' => NULL,
                'created_at' => '2019-05-14 07:43:47',
                'updated_at' => '2019-05-14 07:43:47',
            ),
        ));
        
        
    }
}