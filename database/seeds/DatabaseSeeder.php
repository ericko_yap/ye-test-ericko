<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(CustomersTableSeeder::class);
        $this->call(InvoicesTableSeeder::class);
        $this->call(ItemTypesTableSeeder::class);
        $this->call(OrderDataTableSeeder::class);
        $this->call(OrderDataDetailsTableSeeder::class);
        $this->call(OrderInputsTableSeeder::class);
        $this->call(OrderOutputsTableSeeder::class);
        $this->call(StoragesTableSeeder::class);
        $this->call(UsersTableSeeder::class);
        $this->call(UserLogsTableSeeder::class);
    }
}
