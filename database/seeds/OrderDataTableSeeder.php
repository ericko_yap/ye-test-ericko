<?php

use Illuminate\Database\Seeder;

class OrderDataTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('order_data')->delete();
        
        \DB::table('order_data')->insert(array (
            0 => 
            array (
                'id' => 2,
                'customer_id' => 1,
                'storage_id' => 7,
                'code' => 'ORD00000002',
                'status' => 'COMPLETED',
                'date_input' => '2019-05-12 09:46:58',
                'notes' => '',
                'created_at' => '2019-05-12 09:46:58',
                'updated_at' => '2019-05-12 14:39:09',
                'deleted_at' => NULL,
            ),
            1 => 
            array (
                'id' => 5,
                'customer_id' => 1,
                'storage_id' => 8,
                'code' => 'ORD00000005',
                'status' => 'COMPLETED',
                'date_input' => '2019-05-12 10:17:41',
                'notes' => 'wah gila mantep banget',
                'created_at' => '2019-05-12 10:17:41',
                'updated_at' => '2019-05-12 10:59:13',
                'deleted_at' => NULL,
            ),
            2 => 
            array (
                'id' => 8,
                'customer_id' => 5,
                'storage_id' => 9,
                'code' => 'ORD00000008',
                'status' => 'COMPLETED',
                'date_input' => '2019-05-12 10:19:59',
                'notes' => 'emeizing',
                'created_at' => '2019-05-12 10:19:59',
                'updated_at' => '2019-05-14 02:40:32',
                'deleted_at' => NULL,
            ),
            3 => 
            array (
                'id' => 10,
                'customer_id' => 5,
                'storage_id' => 3,
                'code' => 'ORD00000010',
                'status' => 'COMPLETED',
                'date_input' => '2019-05-12 10:45:58',
                'notes' => 'emeizing',
                'created_at' => '2019-05-12 10:45:58',
                'updated_at' => '2019-05-12 14:42:30',
                'deleted_at' => NULL,
            ),
            4 => 
            array (
                'id' => 11,
                'customer_id' => 20,
                'storage_id' => 3,
                'code' => 'ORD00000011',
                'status' => 'COMPLETED',
                'date_input' => '2019-05-14 01:40:01',
                'notes' => 'Order baru ini terjadi lagi',
                'created_at' => '2019-05-14 01:40:01',
                'updated_at' => '2019-05-14 02:45:47',
                'deleted_at' => NULL,
            ),
            5 => 
            array (
                'id' => 12,
                'customer_id' => 20,
                'storage_id' => 12,
                'code' => 'ORD00000012',
                'status' => 'COMPLETED',
                'date_input' => '2019-05-14 02:46:20',
                'notes' => 'mantap jiwa',
                'created_at' => '2019-05-14 02:46:20',
                'updated_at' => '2019-05-14 02:46:33',
                'deleted_at' => NULL,
            ),
            6 => 
            array (
                'id' => 13,
                'customer_id' => 10,
                'storage_id' => 14,
                'code' => 'ORD00000013',
                'status' => 'COMPLETED',
                'date_input' => '2019-05-14 02:52:57',
                'notes' => 'Woohoo mantap luar biasa',
                'created_at' => '2019-05-14 02:52:57',
                'updated_at' => '2019-05-14 03:34:09',
                'deleted_at' => NULL,
            ),
            7 => 
            array (
                'id' => 14,
                'customer_id' => 21,
                'storage_id' => 2,
                'code' => 'ORD00000014',
                'status' => 'COMPLETED',
                'date_input' => '2019-05-14 03:37:40',
                'notes' => 'HAHAHAHHAH',
                'created_at' => '2019-05-14 03:37:40',
                'updated_at' => '2019-05-14 03:37:58',
                'deleted_at' => NULL,
            ),
            8 => 
            array (
                'id' => 15,
                'customer_id' => 17,
                'storage_id' => 1,
                'code' => 'ORD00000015',
                'status' => 'RENTED',
                'date_input' => '2019-05-14 04:05:30',
                'notes' => 'asdfasdf',
                'created_at' => '2019-05-14 04:05:30',
                'updated_at' => '2019-05-14 04:05:30',
                'deleted_at' => NULL,
            ),
            9 => 
            array (
                'id' => 16,
                'customer_id' => 17,
                'storage_id' => 4,
                'code' => 'ORD00000016',
                'status' => 'RENTED',
                'date_input' => '2019-05-14 06:40:49',
                'notes' => 'Order baru, coba deh',
                'created_at' => '2019-05-14 06:40:49',
                'updated_at' => '2019-05-14 06:40:49',
                'deleted_at' => NULL,
            ),
            10 => 
            array (
                'id' => 22,
                'customer_id' => 16,
                'storage_id' => 8,
                'code' => 'ORD00000022',
                'status' => 'COMPLETED',
                'date_input' => '2019-05-14 07:29:05',
                'notes' => 'LARGE WUIHH',
                'created_at' => '2019-05-14 07:29:05',
                'updated_at' => '2019-05-14 08:02:30',
                'deleted_at' => NULL,
            ),
            11 => 
            array (
                'id' => 24,
                'customer_id' => 10,
                'storage_id' => 13,
                'code' => 'ORD00000024',
                'status' => 'RENTED',
                'date_input' => '2019-05-14 08:00:40',
                'notes' => 'Notes of new order',
                'created_at' => '2019-05-14 08:00:40',
                'updated_at' => '2019-05-14 08:00:40',
                'deleted_at' => NULL,
            ),
            12 => 
            array (
                'id' => 28,
                'customer_id' => 1,
                'storage_id' => 10,
                'code' => 'ORD00000028',
                'status' => 'RENTED',
                'date_input' => '2019-05-14 08:46:41',
                'notes' => 'new order data',
                'created_at' => '2019-05-14 08:46:41',
                'updated_at' => '2019-05-14 08:46:41',
                'deleted_at' => NULL,
            ),
            13 => 
            array (
                'id' => 29,
                'customer_id' => 9,
                'storage_id' => 11,
                'code' => 'ORD00000029',
                'status' => 'RENTED',
                'date_input' => '2019-05-14 08:58:45',
                'notes' => 'asdfasdf',
                'created_at' => '2019-05-14 08:58:45',
                'updated_at' => '2019-05-14 08:58:45',
                'deleted_at' => NULL,
            ),
        ));
        
        
    }
}