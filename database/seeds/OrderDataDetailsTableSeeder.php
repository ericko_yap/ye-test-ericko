<?php

use Illuminate\Database\Seeder;

class OrderDataDetailsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('order_data_details')->delete();
        
        \DB::table('order_data_details')->insert(array (
            0 => 
            array (
                'id' => 1,
                'order_data_id' => 5,
                'item_name' => 'Barang 1',
                'item_type_id' => '2',
                'quantity' => 5,
                'unit' => 'box',
                'notes' => 'mwahahaha',
                'created_at' => '2019-05-14 02:00:00',
                'updated_at' => '2019-05-14 03:00:00',
                'deleted_at' => NULL,
            ),
            1 => 
            array (
                'id' => 2,
                'order_data_id' => 5,
                'item_name' => 'Barang 2',
                'item_type_id' => '3',
                'quantity' => 1000,
                'unit' => 'lembar',
                'notes' => 'Kertas',
                'created_at' => '2019-05-14 01:00:00',
                'updated_at' => '2019-05-14 11:00:00',
                'deleted_at' => NULL,
            ),
            2 => 
            array (
                'id' => 3,
                'order_data_id' => 5,
                'item_name' => 'Container',
                'item_type_id' => '3',
                'quantity' => 5,
                'unit' => 'container',
                'notes' => 'Butuh container besar',
                'created_at' => '2019-05-14 01:00:00',
                'updated_at' => '2019-05-14 11:00:00',
                'deleted_at' => NULL,
            ),
            3 => 
            array (
                'id' => 4,
                'order_data_id' => 22,
                'item_name' => 'kardus rokok',
                'item_type_id' => '2',
                'quantity' => 5,
                'unit' => 'Box',
                'notes' => 'Notes',
                'created_at' => '2019-05-14 07:29:05',
                'updated_at' => '2019-05-14 07:29:05',
                'deleted_at' => NULL,
            ),
            4 => 
            array (
                'id' => 6,
                'order_data_id' => 24,
                'item_name' => 'Rokok',
                'item_type_id' => '3',
                'quantity' => 3,
                'unit' => 'Kotak',
                'notes' => 'mantap jiwa',
                'created_at' => '2019-05-14 08:00:40',
                'updated_at' => '2019-05-14 08:00:40',
                'deleted_at' => NULL,
            ),
            5 => 
            array (
                'id' => 7,
                'order_data_id' => 28,
                'item_name' => 'Rokok',
                'item_type_id' => '1',
                'quantity' => 5,
                'unit' => 'Box',
                'notes' => 'mantap',
                'created_at' => '2019-05-14 08:46:41',
                'updated_at' => '2019-05-14 08:46:41',
                'deleted_at' => NULL,
            ),
            6 => 
            array (
                'id' => 8,
                'order_data_id' => 28,
                'item_name' => 'Document',
                'item_type_id' => '3',
                'quantity' => 12,
                'unit' => 'lembar',
                'notes' => 'asdasdf',
                'created_at' => '2019-05-14 08:46:41',
                'updated_at' => '2019-05-14 08:46:41',
                'deleted_at' => NULL,
            ),
            7 => 
            array (
                'id' => 9,
                'order_data_id' => 28,
                'item_name' => 'asdfasdf',
                'item_type_id' => '1',
                'quantity' => 12,
                'unit' => 'Box',
                'notes' => 'wowowowo',
                'created_at' => '2019-05-14 08:46:41',
                'updated_at' => '2019-05-14 08:46:41',
                'deleted_at' => NULL,
            ),
            8 => 
            array (
                'id' => 10,
                'order_data_id' => 29,
                'item_name' => 'a',
                'item_type_id' => '2',
                'quantity' => 12,
                'unit' => 'asdf',
                'notes' => 'gasdf',
                'created_at' => '2019-05-14 08:58:45',
                'updated_at' => '2019-05-14 08:58:45',
                'deleted_at' => NULL,
            ),
            9 => 
            array (
                'id' => 11,
                'order_data_id' => 29,
                'item_name' => 'ab',
                'item_type_id' => '5',
                'quantity' => 12,
                'unit' => 'Box',
                'notes' => 'gasdf',
                'created_at' => '2019-05-14 08:58:45',
                'updated_at' => '2019-05-14 08:58:45',
                'deleted_at' => NULL,
            ),
            10 => 
            array (
                'id' => 12,
                'order_data_id' => 29,
                'item_name' => 'aaaaa',
                'item_type_id' => '1',
                'quantity' => 11,
                'unit' => 'asdf',
                'notes' => 'gasdf',
                'created_at' => '2019-05-14 08:58:45',
                'updated_at' => '2019-05-14 08:58:45',
                'deleted_at' => NULL,
            ),
        ));
        
        
    }
}