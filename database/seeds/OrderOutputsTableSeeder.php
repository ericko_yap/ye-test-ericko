<?php

use Illuminate\Database\Seeder;

class OrderOutputsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('order_outputs')->delete();
        
        \DB::table('order_outputs')->insert(array (
            0 => 
            array (
                'id' => 1,
                'code' => 'ORD-O-00000001',
                'order_data_id' => 5,
                'order_input_id' => 2,
                'date_output' => '2019-05-12 14:34:17',
                'notes' => 'wow anda luar biasa keren banget',
                'created_at' => '2019-05-12 14:34:17',
                'updated_at' => '2019-05-14 02:53:23',
                'deleted_at' => NULL,
            ),
            1 => 
            array (
                'id' => 2,
                'code' => 'ORD-O-00000002',
                'order_data_id' => 10,
                'order_input_id' => 4,
                'date_output' => '2019-05-12 14:42:30',
                'notes' => 'Create Output new sasdfadf',
                'created_at' => '2019-05-12 14:42:30',
                'updated_at' => '2019-05-13 19:05:25',
                'deleted_at' => NULL,
            ),
            2 => 
            array (
                'id' => 3,
                'code' => 'ORD-O-00000003',
                'order_data_id' => 8,
                'order_input_id' => 3,
                'date_output' => '2019-05-14 02:40:32',
                'notes' => 'Akhirnya keluar juga',
                'created_at' => '2019-05-14 02:40:32',
                'updated_at' => '2019-05-14 02:40:32',
                'deleted_at' => NULL,
            ),
            3 => 
            array (
                'id' => 4,
                'code' => 'ORD-O-00000004',
                'order_data_id' => 11,
                'order_input_id' => 5,
                'date_output' => '2019-05-14 02:45:47',
                'notes' => 'Akhirnya keluar juga yang ini',
                'created_at' => '2019-05-14 02:45:47',
                'updated_at' => '2019-05-14 02:45:47',
                'deleted_at' => NULL,
            ),
            4 => 
            array (
                'id' => 5,
                'code' => 'ORD-O-00000005',
                'order_data_id' => 12,
                'order_input_id' => 6,
                'date_output' => '2019-05-14 02:46:33',
                'notes' => 'Mwahaha keluar juga',
                'created_at' => '2019-05-14 02:46:33',
                'updated_at' => '2019-05-14 02:46:33',
                'deleted_at' => NULL,
            ),
            5 => 
            array (
                'id' => 6,
                'code' => 'ORD-O-00000006',
                'order_data_id' => 13,
                'order_input_id' => 7,
                'date_output' => '2019-05-14 03:34:09',
                'notes' => 'hahahahhhahah',
                'created_at' => '2019-05-14 03:34:09',
                'updated_at' => '2019-05-14 03:34:09',
                'deleted_at' => NULL,
            ),
            6 => 
            array (
                'id' => 7,
                'code' => 'ORD-O-00000007',
                'order_data_id' => 14,
                'order_input_id' => 8,
                'date_output' => '2019-05-14 03:37:58',
                'notes' => 'wuidihhhh',
                'created_at' => '2019-05-14 03:37:58',
                'updated_at' => '2019-05-14 03:37:58',
                'deleted_at' => NULL,
            ),
            7 => 
            array (
                'id' => 8,
                'code' => 'ORD-O-00000008',
                'order_data_id' => 22,
                'order_input_id' => 11,
                'date_output' => '2019-05-14 08:02:30',
                'notes' => 'Keluar dari input sini. aaaa',
                'created_at' => '2019-05-14 08:02:30',
                'updated_at' => '2019-05-14 08:02:48',
                'deleted_at' => NULL,
            ),
        ));
        
        
    }
}