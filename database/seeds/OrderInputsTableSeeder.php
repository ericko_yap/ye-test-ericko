<?php

use Illuminate\Database\Seeder;

class OrderInputsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('order_inputs')->delete();
        
        \DB::table('order_inputs')->insert(array (
            0 => 
            array (
                'id' => 1,
                'code' => 'ORDI00000001',
                'order_data_id' => 2,
                'date_input' => '2019-05-12 09:46:58',
                'notes' => 'gils mantep bangetwow asdfasdfafd',
                'created_at' => '2019-05-12 09:46:58',
                'updated_at' => '2019-05-13 18:50:35',
                'deleted_at' => NULL,
            ),
            1 => 
            array (
                'id' => 2,
                'code' => 'ORDI00000002',
                'order_data_id' => 5,
                'date_input' => '2019-05-12 10:17:41',
                'notes' => 'woohoooooasdfadfaaa',
                'created_at' => '2019-05-12 10:17:41',
                'updated_at' => '2019-05-13 19:04:50',
                'deleted_at' => NULL,
            ),
            2 => 
            array (
                'id' => 3,
                'code' => 'ORDI00000003',
                'order_data_id' => 8,
                'date_input' => '2019-05-12 10:19:59',
                'notes' => 'wow amazingggg',
                'created_at' => '2019-05-12 10:19:59',
                'updated_at' => '2019-05-13 18:50:22',
                'deleted_at' => NULL,
            ),
            3 => 
            array (
                'id' => 4,
                'code' => 'ORDI00000004',
                'order_data_id' => 10,
                'date_input' => '2019-05-12 10:45:58',
                'notes' => 'emeizing',
                'created_at' => '2019-05-12 10:45:58',
                'updated_at' => '2019-05-12 10:45:58',
                'deleted_at' => NULL,
            ),
            4 => 
            array (
                'id' => 5,
                'code' => 'ORD-I-00000005',
                'order_data_id' => 11,
                'date_input' => '2019-05-14 01:40:01',
                'notes' => 'Order baru ini terjadi lagi, 0005',
                'created_at' => '2019-05-14 01:40:01',
                'updated_at' => '2019-05-14 01:43:33',
                'deleted_at' => NULL,
            ),
            5 => 
            array (
                'id' => 6,
                'code' => 'ORD-I-00000006',
                'order_data_id' => 12,
                'date_input' => '2019-05-13 02:46:20',
                'notes' => 'mantap jiwa',
                'created_at' => '2019-05-14 02:46:20',
                'updated_at' => '2019-05-14 02:46:20',
                'deleted_at' => NULL,
            ),
            6 => 
            array (
                'id' => 7,
                'code' => 'ORD-I-00000007',
                'order_data_id' => 13,
                'date_input' => '2019-05-14 02:52:57',
                'notes' => 'Woohoo mantap luar biasa',
                'created_at' => '2019-05-14 02:52:57',
                'updated_at' => '2019-05-14 02:52:57',
                'deleted_at' => NULL,
            ),
            7 => 
            array (
                'id' => 8,
                'code' => 'ORD-I-00000008',
                'order_data_id' => 14,
                'date_input' => '2019-05-14 03:37:40',
                'notes' => 'HAHAHAHHAH',
                'created_at' => '2019-05-14 03:37:40',
                'updated_at' => '2019-05-14 03:37:40',
                'deleted_at' => NULL,
            ),
            8 => 
            array (
                'id' => 9,
                'code' => 'ORD-I-00000009',
                'order_data_id' => 15,
                'date_input' => '2019-05-14 04:05:30',
                'notes' => 'asdfasdf',
                'created_at' => '2019-05-14 04:05:30',
                'updated_at' => '2019-05-14 04:05:30',
                'deleted_at' => NULL,
            ),
            9 => 
            array (
                'id' => 10,
                'code' => 'ORD-I-00000010',
                'order_data_id' => 16,
                'date_input' => '2019-05-14 06:40:49',
                'notes' => 'Order baru, coba deh',
                'created_at' => '2019-05-14 06:40:49',
                'updated_at' => '2019-05-14 06:40:49',
                'deleted_at' => NULL,
            ),
            10 => 
            array (
                'id' => 11,
                'code' => 'ORD-I-00000011',
                'order_data_id' => 22,
                'date_input' => '2019-05-14 07:29:05',
                'notes' => 'LARGE WUIHH',
                'created_at' => '2019-05-14 07:29:05',
                'updated_at' => '2019-05-14 07:29:05',
                'deleted_at' => NULL,
            ),
            11 => 
            array (
                'id' => 13,
                'code' => 'ORD-I-00000013',
                'order_data_id' => 24,
                'date_input' => '2019-05-14 08:00:40',
                'notes' => 'Notes of new order aaaa',
                'created_at' => '2019-05-14 08:00:40',
                'updated_at' => '2019-05-14 08:00:58',
                'deleted_at' => NULL,
            ),
            12 => 
            array (
                'id' => 14,
                'code' => 'ORD-I-00000014',
                'order_data_id' => 28,
                'date_input' => '2019-05-14 08:46:41',
                'notes' => 'new order data',
                'created_at' => '2019-05-14 08:46:41',
                'updated_at' => '2019-05-14 08:46:41',
                'deleted_at' => NULL,
            ),
            13 => 
            array (
                'id' => 15,
                'code' => 'ORD-I-00000015',
                'order_data_id' => 29,
                'date_input' => '2019-05-14 08:58:45',
                'notes' => 'asdfasdf',
                'created_at' => '2019-05-14 08:58:45',
                'updated_at' => '2019-05-14 08:58:45',
                'deleted_at' => NULL,
            ),
        ));
        
        
    }
}