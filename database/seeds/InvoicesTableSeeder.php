<?php

use Illuminate\Database\Seeder;

class InvoicesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('invoices')->delete();
        
        \DB::table('invoices')->insert(array (
            0 => 
            array (
                'id' => 1,
                'code' => 'INV-00000001',
                'customer_id' => 1,
                'order_input_id' => 2,
                'order_output_id' => 1,
                'total_minutes' => 256,
                'amount' => 160000,
                'currency' => 'IDR',
                'notes' => 'Invoice 1 unpaid back.',
                'status' => 'UNPAID',
                'created_at' => '2019-05-12 16:03:09',
                'updated_at' => '2019-05-13 19:33:58',
                'deleted_at' => NULL,
            ),
            1 => 
            array (
                'id' => 2,
                'code' => 'INV-00000002',
                'customer_id' => 5,
                'order_input_id' => 4,
                'order_output_id' => 2,
                'total_minutes' => 236,
                'amount' => 45000,
                'currency' => 'IDR',
                'notes' => 'Invoice 2 PAID TEST 1345',
                'status' => 'PAID',
                'created_at' => '2019-05-12 16:07:45',
                'updated_at' => '2019-05-13 19:31:07',
                'deleted_at' => NULL,
            ),
            2 => 
            array (
                'id' => 3,
                'code' => 'INV-00000003',
                'customer_id' => 5,
                'order_input_id' => 4,
                'order_output_id' => 2,
                'total_minutes' => 236,
                'amount' => 45000,
                'currency' => 'IDR',
                'notes' => 'What the hell...',
                'status' => 'UNPAID',
                'created_at' => '2019-05-12 16:07:47',
                'updated_at' => '2019-05-12 16:12:24',
                'deleted_at' => NULL,
            ),
            3 => 
            array (
                'id' => 4,
                'code' => 'INV-00000004',
                'customer_id' => 5,
                'order_input_id' => 4,
                'order_output_id' => 2,
                'total_minutes' => 236,
                'amount' => 60000,
                'currency' => 'IDR',
                'notes' => 'Invoice 2 PAID TEST',
                'status' => 'PAID',
                'created_at' => '2019-05-12 16:09:56',
                'updated_at' => '2019-05-12 16:14:05',
                'deleted_at' => '2019-05-12 16:14:05',
            ),
            4 => 
            array (
                'id' => 5,
                'code' => 'INV-00000005',
                'customer_id' => 5,
                'order_input_id' => 3,
                'order_output_id' => 3,
                'total_minutes' => 2420,
                'amount' => 1640000,
                'currency' => 'IDR',
                'notes' => 'Mantap luar biasa!',
                'status' => 'UNPAID',
                'created_at' => '2019-05-14 03:26:50',
                'updated_at' => '2019-05-14 03:26:50',
                'deleted_at' => NULL,
            ),
            5 => 
            array (
                'id' => 6,
                'code' => 'INV-00000006',
                'customer_id' => 20,
                'order_input_id' => 5,
                'order_output_id' => 4,
                'total_minutes' => 65,
                'amount' => 30000,
                'currency' => 'IDR',
                'notes' => 'OK lunas mantap',
                'status' => 'PAID',
                'created_at' => '2019-05-14 03:28:07',
                'updated_at' => '2019-05-14 03:28:07',
                'deleted_at' => NULL,
            ),
            6 => 
            array (
                'id' => 7,
                'code' => 'INV-00000007',
                'customer_id' => 20,
                'order_input_id' => 5,
                'order_output_id' => 4,
                'total_minutes' => 65,
                'amount' => 30000,
                'currency' => 'IDR',
                'notes' => 'invoice already created',
                'status' => 'PAID',
                'created_at' => '2019-05-14 03:29:00',
                'updated_at' => '2019-05-14 03:29:00',
                'deleted_at' => NULL,
            ),
            7 => 
            array (
                'id' => 8,
                'code' => 'INV-00000008',
                'customer_id' => 20,
                'order_input_id' => 6,
                'order_output_id' => 5,
                'total_minutes' => 1440,
                'amount' => 720000,
                'currency' => 'IDR',
                'notes' => 'gls mantappp',
                'status' => 'PAID',
                'created_at' => '2019-05-14 03:33:55',
                'updated_at' => '2019-05-14 03:33:55',
                'deleted_at' => NULL,
            ),
            8 => 
            array (
                'id' => 9,
                'code' => 'INV-00000009',
                'customer_id' => 21,
                'order_input_id' => 8,
                'order_output_id' => 7,
                'total_minutes' => 0,
                'amount' => 0,
                'currency' => 'IDR',
                'notes' => 'Selesai',
                'status' => 'PAID',
                'created_at' => '2019-05-14 03:38:16',
                'updated_at' => '2019-05-14 03:38:16',
                'deleted_at' => NULL,
            ),
            9 => 
            array (
                'id' => 11,
                'code' => 'INV-00000011',
                'customer_id' => 10,
                'order_input_id' => 7,
                'order_output_id' => 6,
                'total_minutes' => 41,
                'amount' => 3500,
                'currency' => 'IDR',
                'notes' => 'wuih dibayar lhoasdfa',
                'status' => 'UNPAID',
                'created_at' => '2019-05-14 08:08:48',
                'updated_at' => '2019-05-14 08:09:58',
                'deleted_at' => NULL,
            ),
        ));
        
        
    }
}