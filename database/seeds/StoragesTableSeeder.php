<?php

use Illuminate\Database\Seeder;

class StoragesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('storages')->delete();
        
        \DB::table('storages')->insert(array (
            0 => 
            array (
                'id' => 1,
                'storage_uid' => 'YES00000001',
                'type' => 'SMALL',
                'capacity' => '1000',
                'description' => 'Coba input description pertama',
                'location' => 'Di depan matamu.',
                'price_per_hour' => 10000,
                'currency' => 'IDR',
                'status' => 'OCCUPIED',
                'created_at' => '2019-05-12 01:30:13',
                'updated_at' => '2019-05-14 04:05:30',
                'deleted_at' => NULL,
            ),
            1 => 
            array (
                'id' => 2,
                'storage_uid' => 'YES00000002',
                'type' => 'SMALL',
                'capacity' => '10000',
                'description' => 'Ini lebih bagus dikit dari sebelumnya, ada AC',
                'location' => 'Di bawah kolong jembatan juga',
                'price_per_hour' => 15000,
                'currency' => 'IDR',
                'status' => 'VACANT',
                'created_at' => '2019-05-12 01:30:54',
                'updated_at' => '2019-05-14 03:38:16',
                'deleted_at' => NULL,
            ),
            2 => 
            array (
                'id' => 3,
                'storage_uid' => 'YES00000003',
                'type' => 'SMALL',
                'capacity' => '10000',
                'description' => 'Ini lebih bagus dikit dari sebelumnya, ada AC',
                'location' => 'Di bawah kolong jembatan juga',
                'price_per_hour' => 15000,
                'currency' => 'IDR',
                'status' => 'VACANT',
                'created_at' => '2019-05-12 01:32:42',
                'updated_at' => '2019-05-14 03:28:07',
                'deleted_at' => NULL,
            ),
            3 => 
            array (
                'id' => 4,
                'storage_uid' => 'YES00000004',
                'type' => 'MEDIUM',
                'capacity' => '30000',
                'description' => 'Ukuran Medium, lumayan kapasitasnya',
                'location' => 'Di kolong jembatan, supaya mudah diakses.',
                'price_per_hour' => 35000,
                'currency' => 'IDR',
                'status' => 'OCCUPIED',
                'created_at' => '2019-05-12 01:33:25',
                'updated_at' => '2019-05-14 06:40:49',
                'deleted_at' => NULL,
            ),
            4 => 
            array (
                'id' => 5,
                'storage_uid' => 'YES00000005',
                'type' => 'LARGE',
                'capacity' => '100000',
                'description' => 'Ini lebih bagus dikit dari sebelumnya, ada AC lebih gede',
                'location' => 'Di bawah kolong jembatan juga, somehow muat aja',
                'price_per_hour' => 50000,
                'currency' => 'IDR',
                'status' => 'VACANT',
                'created_at' => '2019-05-12 01:37:15',
                'updated_at' => '2019-05-12 01:37:15',
                'deleted_at' => NULL,
            ),
            5 => 
            array (
                'id' => 6,
                'storage_uid' => 'YES00000006',
                'type' => 'LARGE',
                'capacity' => '80000',
                'description' => 'Ini lebih bagus dikit dari sebelumnya, ada AC lebih gede, tapi lebih kecil',
                'location' => 'Di bawah kolong jembatan juga, somehow muat aja',
                'price_per_hour' => 40000,
                'currency' => 'IDR',
                'status' => 'VACANT',
                'created_at' => '2019-05-12 01:40:23',
                'updated_at' => '2019-05-13 17:14:09',
                'deleted_at' => '2019-05-13 17:14:09',
            ),
            6 => 
            array (
                'id' => 7,
                'storage_uid' => 'YES00000007',
                'type' => 'LARGE',
                'capacity' => '80000',
                'description' => 'Ini lebih bagus dikit dari sebelumnya, ada AC lebih gede, tapi lebih kecil',
                'location' => 'Di bawah kolong jembatan juga, somehow muat aja',
                'price_per_hour' => 40000,
                'currency' => 'IDR',
                'status' => 'VACANT',
                'created_at' => '2019-05-12 01:40:25',
                'updated_at' => '2019-05-13 17:14:18',
                'deleted_at' => '2019-05-13 17:14:18',
            ),
            7 => 
            array (
                'id' => 8,
                'storage_uid' => 'YES00000008',
                'type' => 'LARGE',
                'capacity' => '80000',
                'description' => 'Ini lebih bagus dikit dari sebelumnya, ada AC lebih gede, tapi lebih kecil',
                'location' => 'Di bawah kolong jembatan juga, somehow muat aja',
                'price_per_hour' => 40000,
                'currency' => 'IDR',
                'status' => 'OCCUPIED',
                'created_at' => '2019-05-12 01:40:26',
                'updated_at' => '2019-05-14 07:29:05',
                'deleted_at' => NULL,
            ),
            8 => 
            array (
                'id' => 9,
                'storage_uid' => 'YES00000009',
                'type' => 'LARGE',
                'capacity' => '80000',
                'description' => 'Ini lebih bagus dikit dari sebelumnya, ada AC lebih gede, tapi lebih kecil',
                'location' => 'Di bawah kolong jembatan juga, somehow muat aja',
                'price_per_hour' => 40000,
                'currency' => 'IDR',
                'status' => 'VACANT',
                'created_at' => '2019-05-12 01:40:27',
                'updated_at' => '2019-05-12 10:19:59',
                'deleted_at' => NULL,
            ),
            9 => 
            array (
                'id' => 10,
                'storage_uid' => 'YES00000010',
                'type' => 'LARGE',
                'capacity' => '80000',
                'description' => 'Ini lebih bagus dikit dari sebelumnya, ada AC lebih gede, tapi lebih kecil',
                'location' => 'Di bawah kolong jembatan juga, somehow muat aja',
                'price_per_hour' => 40000,
                'currency' => 'IDR',
                'status' => 'OCCUPIED',
                'created_at' => '2019-05-12 01:40:28',
                'updated_at' => '2019-05-14 08:46:41',
                'deleted_at' => NULL,
            ),
            10 => 
            array (
                'id' => 11,
                'storage_uid' => 'YES00000011',
                'type' => 'LARGE',
                'capacity' => '80000',
                'description' => 'Ini lebih bagus dikit dari sebelumnya, ada AC lebih gede, tapi lebih kecil',
                'location' => 'Di bawah kolong jembatan juga, somehow muat aja',
                'price_per_hour' => 40000,
                'currency' => 'IDR',
                'status' => 'OCCUPIED',
                'created_at' => '2019-05-12 01:40:28',
                'updated_at' => '2019-05-14 08:58:45',
                'deleted_at' => NULL,
            ),
            11 => 
            array (
                'id' => 12,
                'storage_uid' => 'YES00000012',
                'type' => 'MEDIUM',
                'capacity' => '45000',
                'description' => 'Ini lebih bagus dikit dari sebelumnya, ada AC lebih gede, tapi lebih kecil',
                'location' => 'Di bawah kolong jembatan juga, somehow muat aja',
                'price_per_hour' => 30000,
                'currency' => 'IDR',
                'status' => 'VACANT',
                'created_at' => '2019-05-12 02:12:32',
                'updated_at' => '2019-05-14 03:33:55',
                'deleted_at' => NULL,
            ),
            12 => 
            array (
                'id' => 13,
                'storage_uid' => 'YES00000013',
                'type' => 'MEDIUM',
                'capacity' => '45000',
                'description' => 'Ini lebih bagus dikit dari sebelumnya, ada AC lebih gede, tapi lebih kecil',
                'location' => 'Di bawah kolong jembatan juga, somehow muat aja',
                'price_per_hour' => 30000,
                'currency' => 'IDR',
                'status' => 'OCCUPIED',
                'created_at' => '2019-05-12 02:13:01',
                'updated_at' => '2019-05-14 08:00:40',
                'deleted_at' => NULL,
            ),
            13 => 
            array (
                'id' => 14,
                'storage_uid' => 'YES00000014',
                'type' => 'SMALL',
                'capacity' => '5000',
                'description' => 'Ini kecil banget, tolong diperbesar',
                'location' => 'Di bawah kolong jembatan, tapi di belakang-belakang nya.',
                'price_per_hour' => 3500,
                'currency' => 'IDR',
                'status' => 'VACANT',
                'created_at' => '2019-05-12 02:14:29',
                'updated_at' => '2019-05-14 02:52:57',
                'deleted_at' => NULL,
            ),
            14 => 
            array (
                'id' => 15,
                'storage_uid' => 'YES00000015',
                'type' => 'OTHER',
                'capacity' => '100x100',
                'description' => 'Quite a large room',
                'location' => 'Near pearl harbor',
                'price_per_hour' => 100000,
                'currency' => 'IDR',
                'status' => 'VACANT',
                'created_at' => '2019-05-13 17:31:08',
                'updated_at' => '2019-05-13 17:31:08',
                'deleted_at' => NULL,
            ),
            15 => 
            array (
                'id' => 16,
                'storage_uid' => 'YES00000016',
                'type' => 'LARGE',
                'capacity' => '1000L',
                'description' => 'Big Storage 23',
                'location' => 'Location 1',
                'price_per_hour' => 25000,
                'currency' => 'USD',
                'status' => 'VACANT',
                'created_at' => '2019-05-14 07:48:26',
                'updated_at' => '2019-05-14 07:49:17',
                'deleted_at' => '2019-05-14 07:49:17',
            ),
        ));
        
        
    }
}