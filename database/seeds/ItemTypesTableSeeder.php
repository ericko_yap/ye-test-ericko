<?php

use Illuminate\Database\Seeder;

class ItemTypesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('item_types')->delete();
        
        \DB::table('item_types')->insert(array (
            0 => 
            array (
                'id' => 1,
                'code' => 'ITM0001',
                'type' => 'LIQUID',
                'description' => 'Cairan, apapun bentuknya. Yang bener aja',
                'created_at' => '2019-05-12 01:59:48',
                'updated_at' => '2019-05-13 16:44:41',
                'deleted_at' => NULL,
            ),
            1 => 
            array (
                'id' => 2,
                'code' => 'ITM0002',
                'type' => 'PACKAGE',
                'description' => 'Paket, bisa kardus, dus, kertas, apapun lah',
                'created_at' => '2019-05-12 02:00:10',
                'updated_at' => '2019-05-12 02:02:48',
                'deleted_at' => NULL,
            ),
            2 => 
            array (
                'id' => 3,
                'code' => 'ITM0003',
                'type' => 'DOCUMENT',
                'description' => 'Dokumen, biasanya kertas atau pakai USB. Sekarang paperless.',
                'created_at' => '2019-05-12 02:00:24',
                'updated_at' => '2019-05-13 16:45:01',
                'deleted_at' => NULL,
            ),
            3 => 
            array (
                'id' => 4,
                'code' => 'ITM0004',
                'type' => 'CONTAINER',
                'description' => 'Dokumen, biasanya kertas atau pakai USB',
                'created_at' => '2019-05-12 02:13:45',
                'updated_at' => '2019-05-13 16:36:00',
                'deleted_at' => '2019-05-13 16:36:00',
            ),
            4 => 
            array (
                'id' => 5,
                'code' => 'ITM0005',
                'type' => 'SOLID LIQUID',
                'description' => 'All liquids, no exceptions',
                'created_at' => '2019-05-13 16:41:44',
                'updated_at' => '2019-05-13 16:41:44',
                'deleted_at' => NULL,
            ),
            5 => 
            array (
                'id' => 6,
                'code' => 'ITM0006',
                'type' => 'BESI',
                'description' => 'keras banget',
                'created_at' => '2019-05-14 07:46:21',
                'updated_at' => '2019-05-14 07:46:51',
                'deleted_at' => '2019-05-14 07:46:51',
            ),
        ));
        
        
    }
}