<?php

use Illuminate\Database\Seeder;

class CustomersTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('customers')->delete();
        
        \DB::table('customers')->insert(array (
            0 => 
            array (
                'id' => 1,
                'id_number' => '3172023007920002',
                'name' => 'Ericko Yap',
                'email' => 'ericko.yap@gmail.com',
                'address' => 'Trimezia 3 no. 15, Gading Serpong, Tangerang',
                'phone_number' => '081807788992',
                'created_at' => '2019-05-11 02:15:52',
                'updated_at' => '2019-05-11 02:15:52',
                'deleted_at' => NULL,
            ),
            1 => 
            array (
                'id' => 3,
                'id_number' => '11111111133',
                'name' => 'ey4',
                'email' => 'ey4@ey.com',
                'address' => 'Somewhere in this earth 5',
                'phone_number' => '1234567892',
                'created_at' => '2019-05-11 02:29:12',
                'updated_at' => '2019-05-12 01:48:32',
                'deleted_at' => '2019-05-12 01:48:32',
            ),
            2 => 
            array (
                'id' => 5,
                'id_number' => '111111111',
                'name' => 'ey3',
                'email' => 'ey3@ey.com',
                'address' => 'Somewhere in this earth 2',
                'phone_number' => '1234567890',
                'created_at' => '2019-05-11 02:30:35',
                'updated_at' => '2019-05-13 15:25:42',
                'deleted_at' => NULL,
            ),
            3 => 
            array (
                'id' => 6,
                'id_number' => '1234685',
                'name' => 'a',
                'email' => 'a@b.com',
                'address' => 'address 1',
                'phone_number' => '23145678',
                'created_at' => '2019-05-13 10:12:44',
                'updated_at' => '2019-05-13 15:03:31',
                'deleted_at' => '2019-05-13 15:03:31',
            ),
            4 => 
            array (
                'id' => 7,
                'id_number' => '234235367',
                'name' => 'test',
                'email' => 'test@test',
                'address' => 'asdfasdf',
                'phone_number' => '234343536768',
                'created_at' => '2019-05-13 10:25:41',
                'updated_at' => '2019-05-13 15:03:24',
                'deleted_at' => '2019-05-13 15:03:24',
            ),
            5 => 
            array (
                'id' => 8,
                'id_number' => '1245678',
                'name' => 'xxx',
                'email' => 'test2@test',
                'address' => 'asfdasdf',
                'phone_number' => '1243567',
                'created_at' => '2019-05-13 10:31:52',
                'updated_at' => '2019-05-13 15:36:25',
                'deleted_at' => '2019-05-13 15:36:25',
            ),
            6 => 
            array (
                'id' => 9,
                'id_number' => 'asdfasdf',
                'name' => 'gils',
                'email' => 'a@b.com',
                'address' => 'adsfasfd',
                'phone_number' => '123456',
                'created_at' => '2019-05-13 10:33:17',
                'updated_at' => '2019-05-13 10:33:17',
                'deleted_at' => NULL,
            ),
            7 => 
            array (
                'id' => 10,
                'id_number' => '1234566',
                'name' => 'asdf',
                'email' => 'test3@test',
                'address' => 'asdfasf',
                'phone_number' => '25467',
                'created_at' => '2019-05-13 10:33:47',
                'updated_at' => '2019-05-13 10:33:47',
                'deleted_at' => NULL,
            ),
            8 => 
            array (
                'id' => 11,
                'id_number' => '2341251234',
                'name' => 'eric1 eric1',
                'email' => 'the.ericko.mail+1@gmail.com',
                'address' => 'asdfasdf asdfasgasdfas fasda sdgagasdfasdga sf sadfad asfasdf asdf asdf',
                'phone_number' => '2435324',
                'created_at' => '2019-05-13 12:25:20',
                'updated_at' => '2019-05-13 12:25:20',
                'deleted_at' => NULL,
            ),
            9 => 
            array (
                'id' => 12,
                'id_number' => 'sadfaklsdf',
                'name' => 'mantap jiwa',
                'email' => 'a@x.com',
                'address' => 'asdfjaksf',
                'phone_number' => '5234',
                'created_at' => '2019-05-13 12:25:54',
                'updated_at' => '2019-05-13 12:25:54',
                'deleted_at' => NULL,
            ),
            10 => 
            array (
                'id' => 13,
                'id_number' => '135341',
                'name' => 'woohoooo',
                'email' => 'mantap@keren',
                'address' => 'addresss',
                'phone_number' => '1831801',
                'created_at' => '2019-05-13 12:26:26',
                'updated_at' => '2019-05-13 12:26:26',
                'deleted_at' => NULL,
            ),
            11 => 
            array (
                'id' => 14,
                'id_number' => '1251234',
                'name' => 'wuidih',
                'email' => 'the.ericko.mail@gmail.com',
                'address' => 'Trimezia 3 no. 15',
                'phone_number' => '1234567',
                'created_at' => '2019-05-13 12:30:41',
                'updated_at' => '2019-05-13 12:30:41',
                'deleted_at' => NULL,
            ),
            12 => 
            array (
                'id' => 15,
                'id_number' => '2352342',
                'name' => 'wuidihhh',
                'email' => 'mantap@jiwa',
                'address' => 'asdfkasldfk',
                'phone_number' => '13454753',
                'created_at' => '2019-05-13 12:32:46',
                'updated_at' => '2019-05-13 12:32:46',
                'deleted_at' => NULL,
            ),
            13 => 
            array (
                'id' => 16,
                'id_number' => '25261345',
                'name' => 'wuihhh',
                'email' => 'wui@hh',
                'address' => 'sadfkalsfjdl',
                'phone_number' => '4562345',
                'created_at' => '2019-05-13 12:35:16',
                'updated_at' => '2019-05-13 12:35:16',
                'deleted_at' => NULL,
            ),
            14 => 
            array (
                'id' => 17,
                'id_number' => 'askdfjalskdf',
                'name' => 'mwahahah',
                'email' => 'x@y.com',
                'address' => 'asdfkjalsdkf',
                'phone_number' => '583281',
                'created_at' => '2019-05-13 12:41:30',
                'updated_at' => '2019-05-13 12:41:30',
                'deleted_at' => NULL,
            ),
            15 => 
            array (
                'id' => 18,
                'id_number' => '23850235',
                'name' => 'kerengila',
                'email' => 'u@y.com',
                'address' => 'asdkfasdfj',
                'phone_number' => '4536732',
                'created_at' => '2019-05-13 12:42:42',
                'updated_at' => '2019-05-13 12:42:42',
                'deleted_at' => NULL,
            ),
            16 => 
            array (
                'id' => 19,
                'id_number' => '2480245',
                'name' => 'eric5 eric5',
                'email' => 'unique_boyz_92@hotmail.com',
                'address' => 'sdfghsdfgsdfg',
                'phone_number' => '2048201',
                'created_at' => '2019-05-13 12:43:02',
                'updated_at' => '2019-05-14 07:43:03',
                'deleted_at' => '2019-05-14 07:43:03',
            ),
            17 => 
            array (
                'id' => 20,
                'id_number' => '12341234',
                'name' => 'Ericko Yap',
                'email' => 'the.ericko.mail@gmail.comaa',
                'address' => 'Trimezia 3 no. 15234',
                'phone_number' => '12541341',
                'created_at' => '2019-05-13 12:45:45',
                'updated_at' => '2019-05-14 07:44:08',
                'deleted_at' => NULL,
            ),
            18 => 
            array (
                'id' => 21,
                'id_number' => '11111111',
                'name' => 'baru',
                'email' => 'baru@baru',
                'address' => '4810',
                'phone_number' => '18346435',
                'created_at' => '2019-05-13 15:26:01',
                'updated_at' => '2019-05-14 07:43:54',
                'deleted_at' => '2019-05-14 07:43:54',
            ),
        ));
        
        
    }
}