<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTableOrderDataDetails extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_data_details', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('order_data_id');
            $table->string('item_name')->default('');
            $table->string('item_type_id');
            $table->integer('quantity')->default(0);
            $table->string('unit')->default('');
            $table->text('notes');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_data_details');
    }
}
