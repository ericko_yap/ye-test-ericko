<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTableStorages extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('storages', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('storage_uid')->default('');
            $table->enum('type',['SMALL', 'MEDIUM', 'LARGE', 'OTHER'])->default('SMALL');
            $table->string('capacity')->default('');
            $table->text('description')->nullable();
            $table->text('location')->nullable();
            $table->integer('price_per_hour')->default(0);
            $table->string('currency')->default('IDR');
            $table->enum('status', ['VACANT', 'OCCUPIED'])->default('VACANT');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('storages');
    }
}
