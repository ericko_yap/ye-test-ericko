<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTableInvoices extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invoices', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('code')->nullable();
            $table->bigInteger('customer_id');
            $table->bigInteger('order_input_id');
            $table->bigInteger('order_output_id');
            $table->integer('total_minutes')->default(1);
            $table->integer('amount')->default(0);
            $table->string('currency')->default('IDR');
            $table->text('notes')->nullable();
            $table->enum('status',['UNPAID', 'PAID'])->default('UNPAID');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('invoices');
    }
}
